//
//	FacebookExporter.m
//	FacebookExporter
//
//	Created by Sean Farley on 3/17/09.
//	Copyright seanfarley.org 2009. All rights reserved.
//

#import <Log.h>
#import "FacebookExporter.h"
#import "GrowlBridge.h"

// Sparkle
#import "SparkleBridge.h"

// Bridges
#import "FacebookBridge.h"
#import "ApertureBridge.h"
#import "FacebookGUI.h"

// Data model
#import "FBUser.h"
#import "FBImage.h"
#import "FBAlbum.h"

// FB States
#import "FacebookContext.h"
#import "FacebookState.h"
#import "FBLoggedIn.h"
#import "FBLoggedOut.h"

// Aperture States
#import "ApertureContext.h"
#import "ApertureState.h"
#import "ApertureLoadedAlbums.h"
#import "ApertureLoadingAlbums.h"

#import "Alerter.h"


@implementation FacebookExporter

//---------------------------------------------------------
// initWithAPIManager:
//
// This method is called when a plug-in is first loaded, and
// is a good point to conduct any checks for anti-piracy or
// system compatibility. This is also your only chance to
// obtain a reference to Aperture's export manager. If you
// do not obtain a valid reference, you should return nil.
// Returning nil means that a plug-in chooses not to be accessible.
//---------------------------------------------------------

 - (id)initWithAPIManager:(id<PROAPIAccessing>)apiManager
{
	if ((self = [super init]))
	{		
		_apiManager	= apiManager;
		_exportManager = [[_apiManager apiForProtocol:@protocol(ApertureExportManager)] retain];
		if (!_exportManager)
			return nil;
		
		_progressLock = [[NSLock alloc] init];
		
		self.tempDirectoryPath = [NSString stringWithFormat:@"%@ApertureFBExport", NSTemporaryDirectory()];
	}
	
	return self;
}

- (void)dealloc
{
	// Release the top-level objects from the nib.
	[_topLevelNibObjects makeObjectsPerformSelector:@selector(release)];
	[_topLevelNibObjects release];
	
	[_progressLock release];
	[_exportManager release];
	
	// CoreData
	[managedObjectContext release], managedObjectContext = nil;
    [persistentStoreCoordinator release], persistentStoreCoordinator = nil;
    [managedObjectModel release], managedObjectModel = nil;
	
	[super dealloc];
}


#pragma mark -
// UI Methods
#pragma mark UI Methods

- (NSView *)settingsView
{
	if (nil == settingsView)
	{
		// Load the nib using NSNib, and retain the array of top-level objects so we can release
		// them properly in dealloc
		NSBundle *myBundle = [NSBundle bundleForClass:[self class]];
		NSNib *myNib = [[NSNib alloc] initWithNibNamed:@"FacebookExporter" bundle:myBundle];
		if ([myNib instantiateNibWithOwner:self topLevelObjects:&_topLevelNibObjects])
		{
			[_topLevelNibObjects retain];
		}
		[myNib release];
	}
	
	return settingsView;
}

- (NSView *)firstView
{
	return firstView;
}

- (NSView *)lastView
{
	return lastView;
}

- (void)willBeActivated
{
	// This needs to be done so I can call 'stopModal'
	[self performSelectorOnMainThread: @selector(checkForUpdateAndStopModal) withObject: nil waitUntilDone: NO];
}

- (void)willBeDeactivated
{
	[[FacebookBridge instance] cleanup];
	[[Log instance] cleanup];
}

- (void)checkForUpdateAndStopModal {
	
	// Init kFBExportCheckForUpdatesAtStart
	if([[NSUserDefaults standardUserDefaults] valueForKey:kFBExportCheckForUpdatesAtStart] == nil)
		[[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool: YES] forKey:kFBExportCheckForUpdatesAtStart];

// Make separate thread if figure out how to make sparkle work in dialog mode
//	if([[NSUserDefaults standardUserDefaults] boolForKey:kFBExportCheckForUpdatesAtStart])
//		[[SparkleBridge updater] checkForUpdatesInBackground];

	// Could perform a wait until the sparkle finishes checking
	// but initial tests seem that I don't need to yet
	[self performSelectorOnMainThread: @selector(initSingletons) withObject: nil waitUntilDone: NO];
}

- (void)initSingletons {
		
	// Initialize Growl
	[GrowlApplicationBridge setGrowlDelegate:[GrowlBridge instance]];
	
	// Initialize FacebookBridge
	[FacebookBridge instance].plugin = self;
	[FacebookBridge instance].context = [self managedObjectContext];
	[FacebookBridge instance].fbContext.state = [FBLoggedOut instance];
	
	// Initialize ApertureBridge
	[ApertureBridge instance].context = [self managedObjectContext];		
	[ApertureBridge instance].apContext.state = [ApertureLoadingAlbums instance];
	[ApertureBridge instance].exportManager = _exportManager;
	
	// Initialize FacebookGUI
	// Just takes the project name from the first image
	// Maybe there is a better way
	NSDictionary *props = [_exportManager propertiesWithoutThumbnailForImageAtIndex:0];
	[FacebookGUI instance].window = [_exportManager window];
	// Initialize default album
	[FBAlbum albumFactory:nil
					 name:[props valueForKey:kExportKeyProjectName]
				  created:nil
					 size:[NSNumber numberWithInt:0]
					 user:nil
   inManagedObjectContext:[self managedObjectContext]];
	
	[[ApertureBridge instance] loadPhotos];
}

#pragma mark
// Aperture UI Controls
#pragma mark Aperture UI Controls

- (BOOL)allowsOnlyPlugInPresets
{
	if([[NSUserDefaults standardUserDefaults] valueForKey:kFBExportAllowVersionControl] == nil)
		[[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool: YES] forKey:kFBExportAllowVersionControl];
	
	return [[NSUserDefaults standardUserDefaults] boolForKey:kFBExportAllowVersionControl];	
}

- (BOOL)allowsMasterExport
{
	return NO;	
}

- (BOOL)allowsVersionExport
{
	return YES;
}

- (BOOL)wantsFileNamingControls
{
	return NO;	
}

- (void)exportManagerExportTypeDidChange
{
	
}


#pragma mark -
// Save Path Methods
#pragma mark Save/Path Methods

- (BOOL)wantsDestinationPathPrompt
{
	return NO;
}

- (NSString *)destinationPath
{
	return [self defaultDirectory];
}

- (NSString *)defaultDirectory
{
	return self.tempDirectoryPath;
}


#pragma mark -
// Export Process Methods
#pragma mark Export Process Methods

- (void)exportManagerShouldBeginExport
{
	if([[FacebookBridge instance] checkErrorBeforeExport])
		return;

	[Log debugLog:@"Temp path: %@", self.tempDirectoryPath];
	[Log debugLog:@"--------Export started--------"];
	
	[self lockProgress];
	exportProgress.currentValue = 0;
	exportProgress.totalValue = [_exportManager imageCount];
	exportProgress.indeterminateProgress = YES;
	exportProgress.message = [@"Beginning to resize photos..." retain];
	[self unlockProgress];
	
	[[FacebookGUI instance] createAlbumFromGUI];
}

- (void)exportManagerWillBeginExportToPath:(NSString *)path
{
	[self lockProgress];
	exportProgress.message = [@"Beginning to resize photos..." retain];
	[self unlockProgress];
}

- (BOOL)exportManagerShouldExportImageAtIndex:(unsigned)index
{
	return YES;
}

- (void)exportManagerWillExportImageAtIndex:(unsigned)index
{
}

- (BOOL)exportManagerShouldWriteImageData:(NSData *)imageData toRelativePath:(NSString *)path forImageAtIndex:(unsigned)index
{	
	FBImage *i = [FBImage imageAtIndex:[NSNumber numberWithInt:index] inManagedObjectContext:[FacebookBridge instance].context];
// TODO: Check this shouldUpload value everywhere
	return [i.shouldUpload boolValue];	
}

- (void)exportManagerDidWriteImageDataToRelativePath:(NSString *)relativePath forImageAtIndex:(unsigned)index
{
	FBImage *i = [FBImage imageAtIndex:[NSNumber numberWithInt:index] inManagedObjectContext:[self managedObjectContext]];
	i.path = [NSString stringWithFormat:@"%@/%@",self.tempDirectoryPath,relativePath];
	
	[self lockProgress];
	[exportProgress.message release];
	exportProgress.currentValue++;
	exportProgress.message = [[NSString stringWithFormat:@"Step 1: Preparing Image %d of %d...", exportProgress.currentValue, exportProgress.totalValue] retain];
	
	
	[GrowlBridge notify:[NSString stringWithFormat:@"Preparing Image %d of %d", exportProgress.currentValue, exportProgress.totalValue]
			message:[NSString stringWithFormat:@"Resized %@", i.name]
			   icon:[i.thumbnail retain]];
	
	[Log infoLog:@"Preparing Image (%@) %d of %d", i.name, exportProgress.currentValue, exportProgress.totalValue];
	
	[self unlockProgress];
}

- (void)exportManagerDidFinishExport
{
	[self lockProgress];
	exportProgress.indeterminateProgress = YES;
	exportProgress.currentValue = 0;
	exportProgress.totalValue = [_exportManager imageCount];
	exportProgress.message = [@"Beginning Uploading..." retain];
	[self unlockProgress];
	
	// Start the actual uploading
	[FacebookBridge instance].doneResize = YES;
	[[FacebookBridge instance] startUploadingImages];
}

- (void)exportManagerDidFinishUpload
{
	FBAlbum *a = [[[FacebookGUI instance].albumArray selectedObjects] lastObject];

	[GrowlBridge notify:@"Export is complete" message:@"Click here to see album in Facebook" clickContext:a.link];
	[Log debugLog:@"--------Export finished successfully--------"];
	[_exportManager shouldFinishExport];
}

- (void)exportManagerShouldCancelExport
{
	[[FacebookBridge instance] cancelAllRequests];
	[Log debugLog:@"Temp path: %@", self.tempDirectoryPath];
	[Log debugLog:@"--------Export cancelled--------"];
	[_exportManager shouldCancelExport];
}


#pragma mark -
	// Progress Methods
#pragma mark Progress Methods

- (ApertureExportProgress *)progress
{
	return &exportProgress;
}

- (void)lockProgress
{
	
	if (!_progressLock)
		_progressLock = [[NSLock alloc] init];
		
	[_progressLock lock];
}

- (void)unlockProgress
{
	[_progressLock unlock];
}

@synthesize tempDirectoryPath;

@synthesize _apiManager;
@synthesize _exportManager;
@synthesize _progressLock;
@synthesize _topLevelNibObjects;
@synthesize firstView;
@synthesize lastView;

@end

@implementation FacebookExporter (CoreData)

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if (persistentStoreCoordinator != nil) {
        return persistentStoreCoordinator;
    }
    
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel: [self managedObjectModel]];    
	
    return persistentStoreCoordinator;
}

- (NSManagedObjectModel *)managedObjectModel {
	if (managedObjectModel != nil) {
        return managedObjectModel;
    }
	
    managedObjectModel = [[NSManagedObjectModel mergedModelFromBundles:[NSArray arrayWithObject:[NSBundle bundleForClass:[self class]]]] retain];
    return managedObjectModel;
}

- (NSManagedObjectContext *)managedObjectContext {    
	if (managedObjectContext != nil) {
		return managedObjectContext;
	}
	
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator: coordinator];
    }
    
    return managedObjectContext;
}

@end
