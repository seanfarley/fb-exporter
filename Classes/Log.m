//
//  Log.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/24/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "Log.h"

static Log *uniqueInstance = nil;

@implementation Log

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
			
			// Probably should make these parameters
			client = asl_open("FacebookExporter", "com.apple.console", ASL_OPT_STDERR);
			
			fd = open([[[NSString stringWithFormat:@"~/Library/Logs/FacebookExporter.log"] stringByExpandingTildeInPath] UTF8String], O_WRONLY | O_CREAT | O_APPEND, 0644);

			asl_add_log_file(client, fd);
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

// TODO: Check all release calls
// Perhaps get rid of most singletons and garbage collection
//- (void)release {}

- (id)autorelease { return self; }

- (void)release {}

- (void)cleanup
{
	asl_remove_log_file(client, fd);
	close(fd);
	asl_close(client);
	
	uniqueInstance = nil;
	
	[super release];
}

+ (Log *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

/* Log levels:
#define ASL_LEVEL_EMERG   0
#define ASL_LEVEL_ALERT   1
#define ASL_LEVEL_CRIT    2
#define ASL_LEVEL_ERR     3
#define ASL_LEVEL_WARNING 4
#define ASL_LEVEL_NOTICE  5
#define ASL_LEVEL_INFO    6
#define ASL_LEVEL_DEBUG   7
 */

- (void)log:(NSString *)message withLevel:(NSUInteger)level {
//	aslmsg msg = asl_new(ASL_TYPE_MSG);
//	asl_set(msg, ASL_KEY_HOST, "host");
//	asl_set(msg, ASL_KEY_MSG, [message UTF8String]);
//	asl_set(msg, ASL_KEY_LEVEL, ASL_STRING_DEBUG);
//	//asl_log(client, msg, level, NULL);
//	asl_send(client, msg);
//	asl_free(msg);
	asl_log(client, NULL, level, "%s", [message UTF8String]);
}

#pragma mark -
#pragma mark Static Methods

+ (void)debugLog:(NSString *)message, ... {
	va_list va;
    NSString *string;
    
    va_start(va, message);
    string = [[[NSString alloc] initWithFormat:message arguments:va] autorelease];
    va_end(va);
	
	[[Log instance] log:string withLevel:ASL_LEVEL_DEBUG];
}

+ (void)infoLog:(NSString *)message, ... {
	va_list va;
    NSString *string;
    
    va_start(va, message);
    string = [[[NSString alloc] initWithFormat:message arguments:va] autorelease];
    va_end(va);
	
	[[Log instance] log:string withLevel:ASL_LEVEL_INFO];
}

+ (void)errorLog:(NSString *)message, ... {
	va_list va;
    NSString *string;
    
    va_start(va, message);
    string = [[[NSString alloc] initWithFormat:message arguments:va] autorelease];
    va_end(va);
	
	[[Log instance] log:string withLevel:ASL_LEVEL_ERR];
}

@end

