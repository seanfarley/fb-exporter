//
//  Log.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/24/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <asl.h>


@interface Log : NSObject {
	aslclient client;
	int fd;
}

- (void)log:(NSString *)message withLevel:(NSUInteger)level;

+ (Log *)instance;
+ (void)debugLog:(NSString *)message, ...;
+ (void)infoLog:(NSString *)message, ...;
+ (void)errorLog:(NSString *)message, ...;

- (void)cleanup;

@end
