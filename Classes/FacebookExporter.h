//
//	FacebookExporter.h
//	FacebookExporter
//
//	Created by Sean Farley on 3/17/09.
//	Copyright seanfarley.org 2009. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#import "ApertureExportManager.h"
#import "ApertureExportPlugIn.h"

@class SUUpdater;

@interface FacebookExporter : NSObject <ApertureExportPlugIn>
{
	// The cached API Manager object, as passed to the -initWithAPIManager: method.
	id _apiManager; 
	
	// The cached Aperture Export Manager object - you should fetch this from the API Manager during -initWithAPIManager:
	NSObject<ApertureExportManager, PROAPIObject> *_exportManager;
	
	// The lock used to protect all access to the ApertureExportProgress structure
	NSLock *_progressLock;
	
	// Top-level objects in the nib are automatically retained - this array
	// tracks those, and releases them
	NSArray *_topLevelNibObjects;
	
	// The structure used to pass all progress information back to Aperture
	ApertureExportProgress exportProgress;

	// Outlets to your plug-ins user interface
	IBOutlet NSView *settingsView;
	IBOutlet NSView *firstView;
	IBOutlet NSView *lastView;
	
	// Coredata stuff
    NSPersistentStoreCoordinator *persistentStoreCoordinator;
    NSManagedObjectModel *managedObjectModel;
    NSManagedObjectContext *managedObjectContext;
	
	// Temp directory
	
	NSString *tempDirectoryPath;
}

@property (readwrite, copy) NSString *tempDirectoryPath;

@property (retain) id _apiManager;
@property (retain) NSObject<ApertureExportManager, PROAPIObject> *_exportManager;
@property (retain) NSLock *_progressLock;
@property (retain) NSArray *_topLevelNibObjects;
@property (retain,getter=firstView) NSView *firstView;
@property (retain,getter=lastView) NSView *lastView;

- (void)exportManagerDidFinishUpload;

@end

@interface FacebookExporter (CoreData)

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator;
- (NSManagedObjectModel *)managedObjectModel;
- (NSManagedObjectContext *)managedObjectContext;

@end

