//
//  ColorTransformer.m
//  Roster
//
//  Created by Sean Farley on 2/7/09.
//  Copyright 2009 LSU. All rights reserved.
//

#import "NSStringTransformer.h"

@implementation NSStringTransformer

+ (Class)transformedValueClass { return [NSColor class]; }
+ (BOOL)allowsReverseTransformation { return YES; }
- (id)transformedValue:(id)value {

	if([value boolValue])
		return @"Revoke";

	return @"Allow";
}

@end
