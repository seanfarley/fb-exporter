//
//  ApertureLoadingAlbums.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "ApertureLoadingAlbums.h"
#import "FacebookGUI.h"


static ApertureLoadingAlbums *uniqueInstance = nil;

@implementation ApertureLoadingAlbums

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (ApertureState *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- changeState {
	[[FacebookGUI instance].imageView setHidden:YES];
	[[FacebookGUI instance].loadingBox setHidden:NO];
	[[FacebookGUI instance].albumLoadProgress startAnimation:self];
	
	return self;
}

@end
