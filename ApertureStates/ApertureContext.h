//
//  ApertureContext.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@class ApertureState;

@interface ApertureContext : NSObject {
	ApertureState *state;
}

@property (readwrite, assign) ApertureState *state;

@end
