//
//  ApertureState.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface ApertureState : NSObject {

}

+ (ApertureState *)instance;

- changeState;

@end
