//
//  ApertureContext.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "ApertureContext.h"
#import "ApertureState.h"


@implementation ApertureContext

@synthesize state;

- (void)setState:(ApertureState *)newState {
	state = newState;
	[state changeState];
}

- init {
	if((self = [super init])) {
		self.state = [ApertureState instance];
	}
	
	return self;
}

@end
