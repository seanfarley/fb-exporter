//
//  FacebookGUI.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FacebookGUI.h"
#import "ApertureBridge.h"
#import "FacebookBridge.h"
#import "SparkleBridge.h"
#import "FacebookContext.h"
#import "FacebookState.h"
#import "FBImageView.h"
#import "FBTagBox.h"
#import "FBClickableImageCell.h"
#import "FacebookExporter.h"
#import "FBTag.h"
#import "FBUser.h"
#import "FBImage.h"
#import "FBAlbum.h"

#import "Alerter.h"

#import <sys/utsname.h>


static FacebookGUI *uniqueInstance = nil;

@implementation FacebookGUI

@synthesize window;
@synthesize signInButton;
@synthesize fbProgress;
@synthesize fbStatus;
@synthesize fbStatusField;
@synthesize fbVersion;
@synthesize imageView;
@synthesize albumLoadProgress;
@synthesize loadingBox;
@synthesize tagBox;
@synthesize tagName;
@synthesize scrollViewNames;
@synthesize tagSignInButton;
@synthesize thumbnailArray;
@synthesize albumArray;
@synthesize friendArray;
@synthesize tagArray;
@synthesize useExistingAlbum;
@synthesize hasPublish;
@synthesize shouldTryOnlyTag;
@synthesize permissionButton;

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (FacebookGUI *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- (void)awakeFromNib {
	[albumLoadProgress startAnimation:self];
	
	[thumbnailArray addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionNew context:nil];
	[tagArray addObserver:self forKeyPath:@"selection" options:NSKeyValueObservingOptionNew context:nil];
	[friendArray addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueObservingOptionNew context:nil];
	
	NSBundle* mainBundle = [NSBundle bundleForClass:[self class]];
	[fbVersion setTitle:[NSString stringWithFormat:@"%@, v%@", [fbVersion title], [mainBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]];
	[mainBundle release];
	
	if([[NSUserDefaults standardUserDefaults] boolForKey:kFBExportCheckForUpdatesAtStart]) {
		[self setNewVersionHyperlink];
	
		[self performSelectorOnMainThread:@selector(checkForUpdate) withObject:nil waitUntilDone:NO];
	}
	
	NSBundle *apertureBundle = [NSBundle bundleWithIdentifier:@"com.apple.Aperture"];
	[Log infoLog:@"Aperture Version: %@", [apertureBundle objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
	[apertureBundle release];
	
	struct utsname u;
	uname(&u);
	
	[Log infoLog:@"OS Version: %s", u.release];
	[Log infoLog:@"Kernel version: %s", u.machine];
}

- (IBAction)signIn:(id)sender {	
	FacebookState *s = [[FacebookBridge instance].fbContext.state handleAction];
	if(s != nil)
		[FacebookBridge instance].fbContext.state = s;
}

- (IBAction)cancelTagBox:(id)sender {
	[imageView updateImage];
	[tagBox setHidden:YES];
}

- (IBAction)saveTagBox:(id)sender {
	[self tagPhoto];
}

- (void)tagPhoto {
	
	FBUser *u = [FBUser mainUser:[FacebookBridge instance].context];
	FBUser *f;
	if([[friendArray selectedObjects] count]) {
		f = [friendArray.selection valueForKey:@"self"];
	} else {
		f = [FBUser userByName:[tagName stringValue] inManagedObjectContext:[FacebookBridge instance].context];
		if(f == nil)
			f = [u friendFactory:@"" name:[tagName stringValue] index:[NSNumber numberWithInt:0] inManagedObjectContext:[FacebookBridge instance].context];
	}
	
	FBImage *image = [thumbnailArray.selection valueForKey:@"self"];

	[image addTag:f xPercent:imageView.xPercent yPercent:imageView.yPercent inManagedObjectContext:[FacebookBridge instance].context];
	
	[imageView updateImage];
	[tagBox setHidden:YES];

	[[ApertureBridge instance] saveTagData:image];
}

- (IBAction)removeTag:(id)sender {
	[tagArray removeObjects:[tagArray selectedObjects]];
	
	FBImage *image = [thumbnailArray.selection valueForKey:@"self"];
	[[ApertureBridge instance] saveTagData:image];
}


- (void)startObserving:(NSNotification *)notification {}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
	if([keyPath isEqualToString:@"arrangedObjects"]) {
		if([[friendArray arrangedObjects] count] == 1)
			[friendArray selectNext:nil];
	} else {
		[imageView updateImage];
		if([tagBox isHidden]) {
			if(object == tagArray && [[tagArray selectedObjects] count] != 0) {
				for(FBTag *t in [tagArray selectedObjects]) {
					[imageView addTagMarkForXPercent:t.x yPercent:t.y];
				}
			}
		}		
	}
}

- (IBAction)tagBoxDoubleClick {
	[self tagPhoto];
}

// Captures the newline in a textbox event FINALLY
- (BOOL)control:(NSControl *)control textView:(NSTextView *)fieldEditor doCommandBySelector:(SEL)commandSelector {

	if(commandSelector == @selector(insertNewline:) && [tagName isEqualTo:control]) {
		[self tagPhoto];
		return YES;		
	} else if(commandSelector == @selector(moveDown:)) {
		[thumbnailArray selectNext:self];
	} else if(commandSelector == @selector(moveUp:)) {
		[thumbnailArray selectPrevious:self];
	} else if(commandSelector == @selector(cancelOperation:) && ([[tagName stringValue] isEqualToString:@""] || [tagName stringValue] == nil)) {
		[tagBox setHidden:YES];
		return YES;
	} else if(commandSelector == @selector(insertNewline:)) {
		return YES;
	}
	
	return NO;
	
}

- (IBAction)settingsClick:(id)sender {
	
    [NSApp beginSheet:settingsPanel 
	   modalForWindow:[self window] 
		modalDelegate:self 
	   didEndSelector:@selector(settingsDidEndSheet:returnCode:contextInfo:) 
		  contextInfo:nil];
}

- (IBAction)settingsClosed:(id)sender {
//	[[FacebookBridge instance] requestPermission];
    [NSApp endSheet:settingsPanel];
}

- (void)settingsDidEndSheet:(NSWindow *)sheet returnCode:(int)returnCode contextInfo:(void *)contextInfo {
	[sheet orderOut:self];
	
	// Save settings
	NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
	
	[defaults synchronize];
}

- (IBAction)checkForUpdates:(id)sender {
//	[[SparkleBridge updater] checkForUpdates:sender];
}

- createAlbumFromGUI {

	if(useExistingAlbum) {
//		[[[[FacebookBridge instance] plugin] _exportManager] shouldBeginExport];
		[FacebookBridge instance].doneAlbum = YES;
		[[FacebookBridge instance] shouldBeginExport];
	} else {
		FBAlbum *a = [FBAlbum defaultAlbum:[FacebookBridge instance].context];

		[[FacebookBridge instance] createAlbum:a.name albumLocation:a.location albumDescription:a.fbDescription albumVisibility:albumVisibility.selectedTag];
	}
	
	return self;
}

- (IBAction)publishStreamClick:(id)sender {
	[[FacebookBridge instance] handlePublishPermission:!self.hasPublish];
}

- (id)infoValueForKey:(NSString *)key {
	NSBundle *bundle = [NSBundle bundleForClass: [self class]];
	if ([[bundle localizedInfoDictionary] objectForKey: key])
		return [[bundle localizedInfoDictionary] objectForKey: key];
	
	return [[bundle infoDictionary] objectForKey: key];
}

- checkForUpdate {
	NSString *version = [self infoValueForKey: @"CFBundleShortVersionString"];
	
	NSURL *URL = [NSURL URLWithString: @"http://seanfarley.org/aperture_version.txt"];
	NSString *latestVersion = [NSString stringWithContentsOfURL: URL encoding: NSASCIIStringEncoding error: nil];
	
	// just ignore if we can't load it
	if (latestVersion == nil) {
		return self;
	}
	
	latestVersion = [latestVersion stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
	if ([version compare: latestVersion options: NSNumericSearch] == NSOrderedAscending) {
		[newVersionTextField setHidden: NO];
	}
	
	[Log debugLog:@"version: %@", version];
	[Log debugLog:@"latestVersion: %@", latestVersion];
	[Log debugLog:@"compared: %d", [version compare: latestVersion options: NSNumericSearch]];
	
	return self;
}

-(void)setNewVersionHyperlink
{
    // both are needed, otherwise hyperlink won't accept mousedown
    [newVersionTextField setAllowsEditingTextAttributes: YES];
    [newVersionTextField setSelectable: YES];
	
    NSURL* url = [NSURL URLWithString: @"http://seanfarley.org/aperture"];
	
	// build the attributed string
    NSMutableAttributedString* string = [[[NSMutableAttributedString alloc] init] autorelease];
	
	NSMutableAttributedString* attrString = [[NSMutableAttributedString alloc] initWithString: @"New version available"];
    NSRange range = NSMakeRange(0, [attrString length]);
	
    [attrString beginEditing];
    [attrString addAttribute:NSLinkAttributeName value:[url absoluteString] range:range];
	
    // make the text appear in blue
    [attrString addAttribute:NSForegroundColorAttributeName value:[NSColor blueColor] range:range];
	
    // next make the text appear with an underline
    [attrString addAttribute:
            NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSSingleUnderlineStyle] range:range];
	
    [attrString endEditing];
	
    [string appendAttributedString: attrString];
	
	// set the font, size, and alignment
	NSFont *font = [NSFont fontWithName:@"Lucida Grande" size:11.0];
	NSDictionary *attributes = [NSDictionary dictionaryWithObject:font forKey:NSFontAttributeName];
	NSRange fullRange = NSMakeRange(0, [string length]);
	[string setAlignment: NSCenterTextAlignment range: fullRange];
	[string addAttributes: attributes range: fullRange];
	
    // set the attributed string to the NSTextField
    [newVersionTextField setAttributedStringValue: string];
}

@end
