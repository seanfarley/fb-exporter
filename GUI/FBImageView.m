/*
 * Facebook Exporter for iPhoto Software License
 * Copyright (c) 2007, Facebook, Inc.
 * All rights reserved.
 * Permission is hereby granted, free of charge, to any person or organization 
 * obtaining a copy of the software and accompanying documentation covered by 
 * this license (which, together with any graphical images included with such 
 * software, are collectively referred to below as the “Software”) to (a) use, 
 * reproduce, display, distribute, execute, and transmit the Software, (b) 
 * prepare derivative works of the Software (excluding any graphical images 
 * included with the Software, which may not be modified or altered), and (c) 
 * permit third-parties to whom the Software is furnished to do so, all 
 * subject to the following:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Facebook, Inc. nor the names of its contributors may 
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

//
//  FBImageView.m
//  FacebookExport
//
//  Created by Josh Wiseman on 1/26/07.
//	Modified for Aperture by Sean Farley on 4/15/07.
//

#import "FBImageView.h"
#import "FacebookGUI.h"

@implementation FBImageView

static const float BORDER_SIZE = 80.f;
static float HALF_BORDER_SIZE;
static const float THICK_BORDER_WIDTH = 4.f;
static const float THIN_BORDER_WIDTH = 2.f;
static float THIN_OFFSET;

static const float IMAGE_FRAME_OFFSET = 8.f;

- (void)awakeFromNib {
	HALF_BORDER_SIZE = (BORDER_SIZE * .5f);
	THIN_OFFSET = (THICK_BORDER_WIDTH*.5f) + (THIN_BORDER_WIDTH*.5f);
	
	lightBlueColor = [[NSColor colorWithCalibratedRed: .85f green: .87f blue: .91f alpha: 1.f] retain];
	darkBlueColor = [[NSColor colorWithCalibratedRed: .25f green: .36f blue: .58f alpha: 1.f] retain];
	
	NSRect borderRect = NSMakeRect(0.f, 0.f, BORDER_SIZE, BORDER_SIZE);
	thickBorderPath = [[NSBezierPath bezierPathWithRect: borderRect] retain];
	[thickBorderPath setLineJoinStyle: NSRoundLineJoinStyle];
	[thickBorderPath setLineWidth: THICK_BORDER_WIDTH];
	
	borderRect = NSMakeRect(THIN_OFFSET, THIN_OFFSET, BORDER_SIZE - 2.f*THIN_OFFSET, BORDER_SIZE - 2.f*THIN_OFFSET);
	thinBorderPath = [[NSBezierPath bezierPathWithRect: borderRect] retain];
	[thinBorderPath setLineJoinStyle: NSRoundLineJoinStyle];
	[thinBorderPath setLineWidth: THIN_BORDER_WIDTH];
	
	tagMarks = [[NSMutableArray alloc] init];

}


- (void)dealloc {
	[lightBlueColor release];
	[darkBlueColor release];
	[thickBorderPath release];
	[thinBorderPath release];
	[tagMarks release];
	[super dealloc];
}

- (NSNumber *)xPercent {
	NSNumber *xPercent = nil;
	if ([tagMarks count] > 0)
		xPercent = (NSNumber *)[(NSDictionary *)[tagMarks objectAtIndex: 0] valueForKey: @"xPercent"];
	return xPercent;
}

- (NSNumber *)yPercent {
	NSNumber *yPercent = nil;
	if ([tagMarks count] > 0)
		yPercent = (NSNumber *)[(NSDictionary *)[tagMarks objectAtIndex: 0] valueForKey: @"yPercent"];
	return yPercent;
}

- (void)updateImage {
	[self clearTagMarks];
	
	// adjust to screen res (pixels) if it wasn't already
	NSImage *image = [self image];
	[self setImage:image];
	
	NSSize frameSize = [self frame].size;
	
	// account for smaller layout (from IB params)
	frameSize.width -= IMAGE_FRAME_OFFSET * 2.f;
	frameSize.height -= IMAGE_FRAME_OFFSET * 2.f;
	float frameRatio = frameSize.width / frameSize.height;
	
	NSSize imageSize = [image size];
	float imageRatio = imageSize.width / imageSize.height;
	
	float scale = 1.f;
	if (frameRatio > imageRatio) {
		// frame wider than image
		scale = frameSize.height / imageSize.height;
		imageRect.size.width = imageSize.width * scale;
		imageRect.size.height = imageSize.height * scale;
		imageRect.origin.y = IMAGE_FRAME_OFFSET;
		imageRect.origin.x = IMAGE_FRAME_OFFSET + frameSize.width * .5f - imageRect.size.width * .5f;
	}
	else {
		// frame taller than image
		scale = frameSize.width / imageSize.width;
		imageRect.size.width = imageSize.width * scale;
		imageRect.size.height = imageSize.height * scale;
		imageRect.origin.x = IMAGE_FRAME_OFFSET;
		imageRect.origin.y = IMAGE_FRAME_OFFSET + frameSize.height * .5f - imageRect.size.height * .5f;
		
		[image setSize:NSMakeSize(frameSize.width, imageSize.height * scale)];
	}
}

- (void)addTagMarkForXPercent:(NSNumber *)x yPercent:(NSNumber *)y {
	NSDictionary *tagMark = (NSDictionary *)[NSDictionary dictionaryWithObjectsAndKeys: x, @"xPercent", y, @"yPercent", nil];
	[tagMarks addObject: tagMark];
	[self setNeedsDisplay: YES];
}

- (void)clearTagMarks {
	[tagMarks removeAllObjects];
	[self setNeedsDisplay: YES];
}

- (void)mouseDown:(NSEvent *)theEvent {
	[tagMarks removeAllObjects];
	
	NSPoint mousePoint = [self convertPoint: [theEvent locationInWindow] fromView: nil];
	
	float xPercent = (mousePoint.x - imageRect.origin.x) / imageRect.size.width;
	float yPercent = (mousePoint.y - imageRect.origin.y) / imageRect.size.height;
		
	if (xPercent < 0.f)
		xPercent = 0.f;
	else if (xPercent > 1.f)
		xPercent = 1.f;
	if (yPercent < 0.f)
		yPercent = 0.f;
	else if (yPercent > 1.f)
		yPercent = 1.f;
	
	[self addTagMarkForXPercent: [NSNumber numberWithFloat: xPercent] yPercent: [NSNumber numberWithFloat: yPercent]];
	
	[[FacebookGUI instance].tagBox setHidden:NO];
	//Hack. Should make showTagBox and closeTagBox methods.
	[[FacebookGUI instance].tagName setStringValue:@""];
	[[[[FacebookGUI instance].tagName cell] cancelButtonCell] performClick:self];
	//End hack.
	[[FacebookGUI instance].window makeKeyAndOrderFront:nil];
	[[FacebookGUI instance].window makeFirstResponder:[FacebookGUI instance].tagName];
}

- (void)drawRect:(NSRect)aRect {
	[super drawRect: aRect];
	
	/*	
	NSBezierPath *tempPath = [NSBezierPath bezierPathWithRect: imageRect];
	[[NSColor redColor] set];
	[tempPath setLineWidth: 2.f];
	[tempPath stroke];
	 */
	
	NSBezierPath *clipPath = [NSBezierPath bezierPathWithRect: imageRect];
	[clipPath addClip];
	
	int i;
	for (i = 0; i < [tagMarks count]; i++) {
		NSDictionary *tagMark = (NSDictionary *)[tagMarks objectAtIndex: i];
		float xPercent = [(NSNumber *)[tagMark valueForKey: @"xPercent"] floatValue];
		float yPercent = [(NSNumber *)[tagMark valueForKey: @"yPercent"] floatValue];
		
		float startPosX = imageRect.origin.x + (xPercent * imageRect.size.width) - HALF_BORDER_SIZE;
		if(startPosX < imageRect.origin.x) startPosX = imageRect.origin.x;
		else if(startPosX > imageRect.origin.x + imageRect.size.width - BORDER_SIZE) startPosX = imageRect.origin.x + imageRect.size.width - BORDER_SIZE;
		
		float startPosY = imageRect.origin.y + (yPercent * imageRect.size.height) - HALF_BORDER_SIZE;
		if(startPosY < imageRect.origin.y) startPosY = imageRect.origin.y;
		else if(startPosY > imageRect.origin.y + imageRect.size.height - BORDER_SIZE) startPosY = imageRect.origin.y + imageRect.size.height - BORDER_SIZE;
		
		NSAffineTransform *transform = [NSAffineTransform transform];
		[transform translateXBy: startPosX yBy: startPosY];
		[transform concat];
		
		[lightBlueColor set];
		[thickBorderPath stroke];
		
		[darkBlueColor set];
		[thinBorderPath stroke];
			
		transform = [NSAffineTransform transform];
		[transform translateXBy: -startPosX yBy: -startPosY];
		[transform concat];
	}
}

@end
