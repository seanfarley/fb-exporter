/*
 * Facebook Exporter for iPhoto Software License
 * Copyright (c) 2007, Facebook, Inc.
 * All rights reserved.
 * Permission is hereby granted, free of charge, to any person or organization 
 * obtaining a copy of the software and accompanying documentation covered by 
 * this license (which, together with any graphical images included with such 
 * software, are collectively referred to below as the “Software”) to (a) use, 
 * reproduce, display, distribute, execute, and transmit the Software, (b) 
 * prepare derivative works of the Software (excluding any graphical images 
 * included with the Software, which may not be modified or altered), and (c) 
 * permit third-parties to whom the Software is furnished to do so, all 
 * subject to the following:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *   this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *   this list of conditions and the following disclaimer in the documentation 
 *   and/or other materials provided with the distribution.
 * - Neither the name of Facebook, Inc. nor the names of its contributors may 
 *   be used to endorse or promote products derived from this software without 
 *   specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE 
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR 
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN 
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
 * POSSIBILITY OF SUCH DAMAGE.
 *
 */

//
//  FBFocusTableView.m
//  FacebookExport
//
//  Created by Josh Wiseman on 3/2/07.
//	Modified for Aperture by Sean Farley on 4/15/07.
//

#import "FBFocusTableView.h"
#import "FacebookGUI.h"


@implementation FBFocusTableView

// Got this code from http://lists.apple.com/archives/Cocoa-dev/2004/Nov/msg00946.html

//- (BOOL)becomeFirstResponder
//{
//	BOOL result = [super becomeFirstResponder];
//	if (result) {
//		if ([[self delegate] respondsToSelector:@selector(shouldBecomeFirstResponder:)]) {
//			result = [[self delegate] shouldBecomeFirstResponder:self];
//		}
//	}
//	return result;
//}
//
//- (BOOL)resignFirstResponder
//{
//	BOOL result = [super resignFirstResponder];
//	if (result) {
//		if ([[self delegate] respondsToSelector:@selector(didResignFirstResponder:)]) {
//			[(id)[self delegate] didResignFirstResponder:self];
//		}
//	}
//	return result;
//}

- (void)keyDown:(NSEvent*)event {
	if([event type] == NSKeyDown) {
		unichar key = [[event characters] characterAtIndex: 0];
		
		switch (key) {
			case 27:
				[[FacebookGUI instance].tagBox setHidden:YES];
				break;
				
			case ' ':
			case '\n':
			case NSEnterCharacter:
			case '\r':
				[[FacebookGUI instance] tagPhoto];
				break;
				
			default:
				[super keyDown:event];
				break;
		}
	} else {	
		[super keyDown:event];
	}
}

@end
