//
//  FacebookGUI.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FBImageView;
@class FBTagBox;
@class FBClickableImageCell;

@interface FacebookGUI : NSObject {
	
	NSWindow *window;
	
	IBOutlet NSButton *signInButton;
	IBOutlet NSProgressIndicator *fbProgress;
	IBOutlet NSTextFieldCell *fbStatus;
	IBOutlet NSTextField *fbStatusField;
	IBOutlet NSTextFieldCell *fbVersion;

	IBOutlet FBImageView *imageView;
	IBOutlet NSProgressIndicator *albumLoadProgress;
	IBOutlet NSBox *loadingBox;
	
	IBOutlet FBTagBox *tagBox;
	IBOutlet NSSearchField *tagName;
	IBOutlet NSScrollView *scrollViewNames;
	IBOutlet NSButton *tagSignInButton;
	
	IBOutlet NSArrayController *thumbnailArray;
	IBOutlet NSArrayController *albumArray;
	IBOutlet NSArrayController *friendArray;
	IBOutlet NSArrayController *tagArray;
	
	IBOutlet NSButton *checkStreamButton;
	
	IBOutlet NSTextField *newVersionTextField;
	
	BOOL useExistingAlbum; //ugh, I don't really want this variable
	BOOL hasPublish;
	BOOL shouldTryOnlyTag;
	
	IBOutlet NSPanel *settingsPanel;
	IBOutlet NSButton *permissionButton;
	
	IBOutlet NSPopUpButton *albumVisibility;
}

@property (readwrite, assign) NSWindow *window;

@property (readwrite, assign) NSButton *signInButton;
@property (readwrite, assign) NSProgressIndicator *fbProgress;
@property (readwrite, assign) NSTextFieldCell *fbStatus;
@property (readwrite, assign) NSTextField *fbStatusField;
@property (readwrite, assign) NSTextFieldCell *fbVersion;

@property (readwrite, assign) FBImageView *imageView;
@property (readwrite, assign) NSProgressIndicator *albumLoadProgress;
@property (readwrite, assign) NSBox *loadingBox;

@property (readwrite, assign) FBTagBox *tagBox;
@property (readwrite, assign) NSSearchField *tagName;
@property (readwrite, assign) NSScrollView *scrollViewNames;
@property (readwrite, assign) NSButton *tagSignInButton;

@property (readwrite, assign) NSArrayController *thumbnailArray;
@property (readwrite, assign) NSArrayController *albumArray;
@property (readwrite, assign) NSArrayController *friendArray;
@property (readwrite, assign) NSArrayController *tagArray;

@property (readwrite, assign) BOOL useExistingAlbum;
@property (readwrite, assign) BOOL hasPublish;
@property (readwrite, assign) BOOL shouldTryOnlyTag;
@property (readwrite, assign) NSButton *permissionButton;

+ (FacebookGUI *)instance;

- (IBAction)signIn:(id)sender;

- (IBAction)cancelTagBox:(id)sender;
- (IBAction)saveTagBox:(id)sender;
- (IBAction)removeTag:(id)sender;
- (IBAction)settingsClick:(id)sender;
- (IBAction)settingsClosed:(id)sender;

- (IBAction)checkForUpdates:(id)sender;

- (IBAction)tagBoxDoubleClick;

- (IBAction)publishStreamClick:(id)sender;

- (void)tagPhoto;
- createAlbumFromGUI;
- checkForUpdate;
-(void)setNewVersionHyperlink;

@end
