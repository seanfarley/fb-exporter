//
//  Alerter.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface Alerter : NSObject {
	NSWindow *window;
}

@property (readwrite, assign) NSWindow *window;

+ (Alerter *)instance;

@end

@interface Alerter (StaticMethods)

+ (NSInteger)showAlert:(NSString*)title withMessage:(NSString*)message;
+ (NSInteger)showUserInfoError;
+ (NSInteger)showMaxPhotosAlert;
+ (NSInteger)showConnectionError;
+ (NSInteger)showAlbumConnectionAlert;
+ (NSInteger)showFriendsConnectionAlert;
+ (NSInteger)showExportVerifyAlert: (NSString *)albumName Count:(NSInteger)count;
+ (NSInteger)showCreateAlbumError;
+ (NSInteger)showMKError;

@end
