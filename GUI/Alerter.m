//
//  Alerter.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "Alerter.h"
#import "FBAlbum.h"

static Alerter *uniqueInstance = nil;

@implementation Alerter

@synthesize window;

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (Alerter *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

@end

@implementation Alerter (StaticMethods)

+ (NSInteger)showExportVerifyAlert: (NSString *)albumName Count:(NSInteger)count {
	NSString *photoString = nil;
	NSString *pendingString = nil;
	if (count == 1) {
		photoString = @"one photo";
		pendingString = @"The photo will be considered pending until you approve it";
	}
	else {
		photoString = [NSString stringWithFormat: @"%d photos", count];
		pendingString = @"The photos will be considered pending until you approve them";
	}
	
	NSAlert *a = [NSAlert alertWithMessageText:[NSString stringWithFormat: @"Press Continue to upload %@ to Facebook.\n\n%@ in the album \"%@.\"  Your pending photos are not visible to anyone else.", photoString, pendingString, albumName] 
								 defaultButton:@"Continue"
							   alternateButton:@"Cancel" 
								   otherButton:nil
					 informativeTextWithFormat:@"I certify that I have the right to distribute these photos and that they do not violate the Terms of Use at http://www.facebook.com/terms.php."];
	
	return [a runModal];
}

+ (NSInteger)showAlert:(NSString*)title withMessage:(NSString*)message {
	[Log infoLog:@"Alert [%@] with message: %@", title, message];
	
	NSAlert *a = [NSAlert alertWithMessageText:title 
								   defaultButton:@"OK"
								 alternateButton:nil 
									 otherButton:nil
					   informativeTextWithFormat:message];
	return [a runModal];
}

+ (NSInteger)showUserInfoError {
	return [Alerter showAlert:@"Connection Error" withMessage:@"There was a problem retrieving your user information."];
}

+ (NSInteger)showMaxPhotosAlert {
	return [Alerter showAlert:@"Too Many Photos"
				  withMessage:[NSString stringWithFormat: @"You may only export %d photos at a time.  Additional photos will cause errors, please cancel and select fewer photos.\n\n", [FBAlbum maxSize]]];

}

+ (NSInteger)showAlbumConnectionAlert {
	return [Alerter showAlert:@"Connection Problems"
				  withMessage:@"There was an error when attempting to retrieve your album list"];
}

+ (NSInteger)showFriendsConnectionAlert {
	return [Alerter showAlert:@"Connection Problems" withMessage:@"There was an error when attempting to retrieve your friend list"];
}

+ (NSInteger)showConnectionError {
	return [Alerter showAlert:@"Connection Error"
				  withMessage:@"There was a problem connecting with Facebook. Check your Internet connection and try again."];
}

+ (NSInteger)showCreateAlbumError {
	return [Alerter showAlert:@"Facebook Error"
				  withMessage:@"There was a problem creating your album with Facebook."];
}

+ (NSInteger)showMKError {
	return [Alerter showAlert:@"Facebook Error"
				  withMessage:@"There was an unexpected problem using the MKAbeFook framework to connect to Facebook. The only option is to quit and restart."];
}

@end
