//
//  FBClickableImageCell.m
//  FacebookExport
//
//  Created by Josh Wiseman on 1/26/07.
//	Modified for Aperture by Sean Farley on 4/15/07.
//

#import "FBClickableImageCell.h"


@implementation FBClickableImageCell

- (void)setValue:(id)value forKey:(NSString *)key {
//	printf("in set key\n");
	[super setValue:value forKey: key];
}

- (void)setImage:(NSImage *)image {
//	printf("setting image!\n");
	[super setImage: image];
}

@end
