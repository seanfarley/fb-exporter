//
//  FBConnectAPI.h
//  FacebookAperture
//
//  Created by Sean Farley on 2/18/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <FacebookAPI.h>


@class FacebookRequestController;

@interface FBGraphAPI : FacebookAPI {
	FacebookRequestController *_connect;
}

@property(nonatomic,assign) FacebookRequestController *connect;

@end
