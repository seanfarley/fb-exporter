//
//  FBConnectRequestQueueAPI.h
//  FacebookAperture
//
//  Created by Sean Farley on 3/9/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FacebookRequestQueueAPI.h"


@interface FBGraphRequestQueueAPI : FacebookRequestQueueAPI {
	NSMutableArray *_requests;
	BOOL _cancelRequestQueue;
	NSInteger _currentRequest;
}

- (void)startNextRequest;

@end
