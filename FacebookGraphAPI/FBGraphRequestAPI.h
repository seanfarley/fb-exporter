//
//  FBConnectRequestAPI.h
//  FacebookAperture
//
//  Created by Sean Farley on 3/9/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FacebookRequestAPI.h"

@protocol FBRequest;

@interface FBGraphRequestAPI : FacebookRequestAPI {
	NSMutableDictionary *_parameters;
	id<FBRequest> r;
}

@end
