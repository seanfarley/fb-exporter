//
//  FBConnectImplementation.h
//  FacebookAperture
//
//  Created by Sean Farley on 3/9/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FBGraphAPI.h"
#import "FBGraphRequestAPI.h"
#import "FBGraphRequestQueueAPI.h"


#define ConcreteFacebookAPI FBGraphAPI
#define ConcreteFacebookRequestAPI FBGraphRequestAPI
#define ConcreteFacebookRequestQueueAPI FBGraphRequestQueueAPI