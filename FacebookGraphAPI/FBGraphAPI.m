//
//  FBConnectAPI.m
//  FacebookAperture
//
//  Created by Sean Farley on 2/18/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FBGraphAPI.h"
#import <FacebookGUI.h>
#import <FacebookRequestController.h>
#import <Log.h>

@implementation FBGraphAPI

@synthesize connect = _connect;

- initWithAPIKey: (NSString *)api delegate:(id)del selector:(SEL)sel {	
	if (!(self = [super init])) {
		return nil;
	}
	
	_delegate = del;
	
	_selector = sel;
	
    _connect = [[FacebookRequestController alloc] init];
    [[self connect] setDelegate:self];
	
	return self;
}

- (NSString *)uid {
	return [_connect uid];
}

- (NSWindow*)login {
	return [_connect loginWithRequiredPermissions:[NSSet setWithObjects:@"offline_access", nil]
					   optionalPermissions:[NSSet setWithObjects:@"publish_stream", nil]
												forSheet:YES
											  showPrompt:YES];
}

- (void)logout {
	[_connect logout];
}

- (NSWindow*)grantExtendedPermission:(NSString *)aString forSheet:(BOOL)forSheet delegate:(id)del selector:(SEL)sel {
	_perm_delegate = del;
	_perm_selector = sel;
	_permission = aString;
	return [_connect requestPermissions:[NSSet setWithObject:aString]
										target:self
									  selector:@selector(statusPermissionResponse:)
									  forSheet:YES];
}

- (BOOL)userLoggedIn {
	if(![_connect isLoggedIn]) {
		[_connect loginWithRequiredPermissions:[NSSet setWithObjects:@"offline_access", nil]
					   optionalPermissions:[NSSet setWithObjects:@"publish_stream", nil]
									  forSheet:NO
									showPrompt:NO];
	}
	return [_connect isLoggedIn];
}

- (id)connection {
	return _connect;
}

#pragma mark -
#pragma mark Delegates

- (void)facebookConnectLoggedIn:(FBConnect*)connect withError:(NSError*)err
{
	// This delegate gets called even if the user clicks 'cancel'
	if([_connect isLoggedIn] && [_delegate respondsToSelector:_selector])
		[_delegate performSelector:_selector withObject:err];
	else if(![self userLoggedIn] && [_delegate respondsToSelector:@selector(userLoginFailed:)])
		[_delegate performSelector:@selector(userLoginFailed:) withObject:err];
}

- (void)facebookConnectLoggedOut:(FBConnect*)connect withError:(NSError*)err
{
  if (err) {
    [Log errorLog:@"couldn't log out of fb servers, at least locally logged out"];
  }

  [Log debugLog:@"logged out"];
}

- (void)statusPermissionResponse:(id<FBRequest>)req
{
	if ([req error]) {
		[Log errorLog:@"perm error: %@ %@", [req error], [[[req error] userInfo] objectForKey:kFBErrorMessageKey]];
		return;
	}
	
	[Log debugLog:@"statusPermissionResponse: %@", [req response]];
	
	NSMutableDictionary *dict = [NSMutableDictionary dictionary];
	if([req response]) {
		for(NSString *key in [req response])
			[dict setObject:[NSNumber numberWithBool:YES] forKey:key];
	} else {
		[dict setObject:[NSNumber numberWithBool:NO] forKey:_permission];
	}

	if([_perm_delegate respondsToSelector:_perm_selector])
		[_perm_delegate performSelector:_perm_selector withObject:dict];
}

@end
