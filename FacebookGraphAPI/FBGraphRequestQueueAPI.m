//
//  FBConnectRequestQueueAPI.m
//  FacebookAperture
//
//  Created by Sean Farley on 3/9/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FBGraphRequestQueueAPI.h"


@implementation FBGraphRequestQueueAPI

- init {
	self = [super init];
	if(self != nil) {
		_requests = [[NSMutableArray alloc] init];
		_cancelRequestQueue = NO;
		_currentRequest = 0;
	}
	return self;
}

- (NSUInteger)count {
	return [_requests count];
}

- (void)addRequest:(FacebookRequestAPI *)request {
	[_requests addObject:request];
}

- (void)startRequestQueue
{
	if(!_currentRequest) {
		 _cancelRequestQueue = NO;
		[self startNextRequest];
	}
}

- (void)startNextRequest
{
	if(_currentRequest <= [_requests count] && _cancelRequestQueue == NO && [_requests count] != 0 )
	{
		NSDictionary *progress = [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:_currentRequest], @"current", [NSNumber numberWithInteger:[_requests count]], @"total", nil];
		
		if([_delegate respondsToSelector:_currentlySendingSelector])
			[_delegate performSelector:_currentlySendingSelector withObject:progress];
		
		[Log debugLog:@"Request %lx in %@ queue started", _currentRequest, _debugName];
		
		[[_requests objectAtIndex:_currentRequest] setDelegate:self];
		[[_requests objectAtIndex:_currentRequest] setSelector:@selector(gotResponse:)];
		[[_requests objectAtIndex:_currentRequest] setApi:_api];
		[[_requests objectAtIndex:_currentRequest] sendRequest];
		
		_currentRequest++;
	}
	else
	{
		_currentRequest = 0;
		[_requests removeAllObjects];
		if([_delegate respondsToSelector:_allRequestsFinishedSelector])
			[_delegate performSelector:_allRequestsFinishedSelector];
	}
		
}

- (void)cancelRequestQueue
{
	if(_currentRequest < [_requests count])
	{
		[[_requests objectAtIndex:_currentRequest] cancelRequest];
		_cancelRequestQueue = YES;
		[_requests removeAllObjects];
	}
}

#pragma mark -
#pragma mark Delegates

- (void)gotResponse:(id)data
{
	[Log debugLog:@"Request %lx in %@ queue completed", _currentRequest-1, _debugName];
	if([_delegate respondsToSelector:_lastRequestResponseSelector])
		[_delegate performSelector:_lastRequestResponseSelector withObject:data];
	
	
	if(_currentRequest < [_requests count] && _cancelRequestQueue == NO && [_requests count] != 0)
	{
		[self startNextRequest];
	}
	else
	{
		_currentRequest = 0;
		[_requests removeAllObjects];
		if([_delegate respondsToSelector:_allRequestsFinishedSelector])
			[_delegate performSelector:_allRequestsFinishedSelector];
	}
		
}

@end
