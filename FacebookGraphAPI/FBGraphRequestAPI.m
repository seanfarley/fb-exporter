//
//  FBConnectRequestAPI.m
//  FacebookAperture
//
//  Created by Sean Farley on 3/9/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FBGraphRequestAPI.h"
#import <FBCocoa/FBCocoa.h>


@implementation FBGraphRequestAPI

- (void)setParameters:(NSMutableDictionary *)parameters {
	_parameters = [parameters retain];
}

- (void)sendRequest {
	NSString *method = [_parameters valueForKey:@"method"];
//	Don't need to remove 'method'
	
	[Log debugLog:@"Sending: %@ %@", method, [_parameters objectForKey:@"query"] ? [NSString stringWithFormat:@"\n%@", [_parameters objectForKey:@"query"]] : @""];
	
	if([_parameters objectForKey:@"image"]) {
		NSImage *image = [[_parameters objectForKey:@"image"] retain];
		[_parameters removeObjectForKey:@"image"];
		
		r = [[(FBConnect*)[_api connection] callMethod:method
									withArguments:_parameters
										withFiles:[NSArray arrayWithObject:image]
										   target:self
										 selector:@selector(gotResponse:)
									scheduleModal:_scheduleLoopIsModal] retain];
	} else {
		r = [[(FBConnect*)[_api connection] callMethod:method
									withArguments:_parameters
										   target:self
										 selector:@selector(gotResponse:)
									scheduleModal:_scheduleLoopIsModal] retain];
	}
}

- (void)cancelRequest {
	if(r) [r cancel];
}

#pragma mark -
#pragma mark Delegates

- (void)gotResponse:(id<FBRequest>)req {
	// Set 'r' to nil so that it won't be deallocated and then cancelled
	r = nil;
	
	if ([req error]) {
		[Log errorLog:@"error: %@ %@", [req error], [[[req error] userInfo] objectForKey:kFBErrorMessageKey]];
		return;
	}
	
	//[Log debugLog:@"gotResponse: %@", [req response]];
	
	id info = [req response];
	if([info isKindOfClass:[NSArray class]] && [(NSArray*)info count] == 1) info = [(NSArray*)[req response] objectAtIndex:0];

	if([_delegate respondsToSelector:_selector])
		[_delegate performSelector:_selector withObject:info];
}

@end
