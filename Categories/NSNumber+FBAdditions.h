//
//  NSNumberAdditions.h
//  ipodTest1
//
//  Created by Jonathan Saggau on 8/31/06.
//  Copyright 2006 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>


@interface NSNumber (NSNumberAdditions)

- (NSString *)timeValue;
+ (NSNumber *)numberWithString:(NSString *)string;

@end