//
// NSNumberAdditions.m
// ipodTest1
//
// Created by Jonathan Saggau on 8/31/06.
// Copyright 2006 __MyCompanyName__. All rights reserved.
//


#import "NSNumber+FBAdditions.h"
#import <Log.h>


@implementation NSNumber (NSNumberAdditions)

- (NSString *)timeValue {
	const int secsPerMin = 60;
	const int secsPerHour = secsPerMin * 60;
	const int secsPerDay = 24 * secsPerHour;
	const char *timeSep = ":";
	const char *secsName = " s";
	const char *minsName = " m";
	const char *hrsName = " h";
	const char *daysName = " days";
	
	double time = [self doubleValue];
	
	int hrs = time/secsPerHour;
	if(hrs > 36) return [NSString stringWithFormat:@"%0.2f%s", time/secsPerDay, daysName];
	if(time < 60.0) return [NSString stringWithFormat:@"%0.2f%s", time, secsName];
	
	time -= hrs*secsPerHour;
	int mins = time/secsPerMin;
	time -= mins*secsPerMin;
	int secs = time;
	
	if(hrs == 0) return [NSString stringWithFormat:@"%d%s%02d%s", mins, timeSep, secs, minsName];
	return [NSString stringWithFormat:@"%d%s%02d%s%02d%s", hrs, timeSep, mins, timeSep, secs, hrsName];
}

// TODO: Maybe this is not needed if JSON is used
+ (NSNumber *)numberWithString:(NSString *)string {
	NSScanner *scanner;
	int scanLocation = 0;

	scanner = [NSScanner scannerWithString:string];
	if ([string hasPrefix:@"$"]) scanLocation = 1;
	// just in case we're given a dollar value

	int intResult;
	[scanner setScanLocation:scanLocation];
	if ([scanner scanInt:&intResult]
		&& ([scanner scanLocation] == [string length] )) {
		return [NSNumber numberWithInt:intResult];
	}

	float floatResult;
	[scanner setScanLocation:scanLocation];
	if ([scanner scanFloat:&floatResult]
		&& ([scanner scanLocation] == [string length] )) {
		return [NSNumber numberWithFloat:floatResult];
	}

	long long longLongResult;
	[scanner setScanLocation:scanLocation];
	if ([scanner scanLongLong:&longLongResult]
		&& ([scanner scanLocation] == [string length] )) {
		return [NSNumber numberWithLongLong:floatResult];
	}

	[Log infoLog:@"WARNING::: Couldn't convert %@ to nsnumber", string];
	return [NSNumber numberWithInt:0];
}

@end

