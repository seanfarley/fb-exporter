//
//  GrowlBridge.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/17/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <Growl/GrowlApplicationBridge.h>


@interface GrowlBridge : NSObject <GrowlApplicationBridgeDelegate> {

}

- (NSDictionary *)registrationDictionaryForGrowl;

+ (GrowlBridge *)instance;
+ (void)notify:(NSString *)title message:(NSString *)message;
+ (void)notify:(NSString *)title message:(NSString *)message clickContext:(id)context;
+ (void)notify:(NSString *)title message:(NSString *)message icon:(NSImage *)iconData;
+ (void)notify:(NSString *)title message:(NSString *)message icon:(NSImage *)iconData clickContext:(id)context;

@end
