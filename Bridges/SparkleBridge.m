//
//  FBApertureUpdater.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/19/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "SparkleBridge.h"


@implementation SparkleBridge

+ (SUUpdater *)updater {
	return [SUUpdater updaterForBundle:[NSBundle bundleForClass:[self class]]];
}

@end
