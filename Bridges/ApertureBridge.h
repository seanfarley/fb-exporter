//
//  ApertureBridge.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "ApertureExportManager.h"
#import "ApertureExportPlugIn.h"

#define kFBPid @"Facebook Pid"
#define kFBTags @"Facebook Tag Data"

@class ApertureContext;
@class FBImage;

@interface ApertureBridge : NSObject {
	ApertureContext *apContext;
	NSManagedObjectContext *context;
	NSObject<ApertureExportManager, PROAPIObject> *exportManager;
}

@property (readwrite, assign) NSManagedObjectContext *context;
@property (readwrite, assign) ApertureContext *apContext;
@property (retain) NSObject<ApertureExportManager, PROAPIObject> *exportManager;

+ (ApertureBridge *)instance;

- (void)saveTagData:(FBImage *)image;
- (void)savePid:(FBImage *)image;
@end

@interface ApertureBridge (Threads)

- (void)loadPhotos;
- (void)loadMetadata;

@end

