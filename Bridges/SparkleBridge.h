//
//  FBApertureUpdater.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/19/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "./Sparkle.framework/Headers/SUUpdater.h"


@interface SparkleBridge : NSObject

+ (SUUpdater *)updater;

@end
