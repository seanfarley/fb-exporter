//
//  FacebookBridge.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/21/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <FacebookAPI.h>

@class FBImage;
@class FacebookContext;
@class FacebookExporter;
@class FacebookAPI;
@class FacebookRequestAPI;
@class FacebookRequestQueueAPI;

#define kFBExportAllowVersionControl @"kFBExportAllowVersionControl"
#define kFBExportCheckForUpdatesAtStart @"kFBExportCheckForUpdatesAtStart"

#define kFBExportAPI @"b1fee63898cd71e1f632b2346a99d488"

@interface FacebookBridge : NSObject {

	FacebookAPI *api;
	NSManagedObjectContext *context;
	FacebookContext *fbContext;
	
	FacebookRequestAPI *userInfoRequest;
	FacebookRequestAPI *albumsRequest;
	FacebookRequestAPI *newAlbumRequest;
	FacebookRequestAPI *friendsRequest;
	FacebookRequestAPI *permissionRequest;
	FacebookRequestAPI *pidRequest;
	
	FacebookRequestQueueAPI *requestImageQueue;
	FacebookRequestQueueAPI *requestTagQueue;
	FBImage *currentImage;
	NSDate *currentTime;
	FacebookExporter *plugin;
	
	BOOL _cancelRequests;
	BOOL _doneResize;
	BOOL _doneAlbum;
	BOOL _doneUploadingImages;
	BOOL _doneUploadingTags;
}


@property (readwrite, assign) FacebookAPI *api;
@property (readwrite, assign) NSManagedObjectContext *context;
@property (readwrite, assign) FacebookContext *fbContext;

@property (readwrite, assign) FacebookExporter *plugin;

@property (readwrite, assign) BOOL doneResize;
@property (readwrite, assign) BOOL doneAlbum;

+ (FacebookBridge *)instance;

- (BOOL)userLoggedIn;
- login:(BOOL)shouldPrompt;
- logout;
- requestAlbums;
- requestFriends;
- requestPermission;
- revokePermission;
- initRequests;
- initPidRequest;
- shouldBeginExport;

- queueImage:(FBImage *)image;
- queueTags:(FBImage *)image;
- startUploadingImages;
- cancelAllRequests;

- createAlbum:(NSString *)name albumLocation:(NSString *)location albumDescription:(NSString *)description albumVisibility:(NSInteger)tag;
- handlePublishPermission:(BOOL)grant;

- userLoginSuccessful:(NSError*)err;

- (BOOL)checkErrorBeforeExport;

- (void)cleanup;
- finishUpload;

@end
