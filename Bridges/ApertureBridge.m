//
//  ApertureBridge.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/25/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "ApertureBridge.h"
#import "ApertureContext.h"
#import "ApertureState.h"
#import "ApertureLoadedAlbums.h"
#import "ApertureLoadingAlbums.h"

#import "FBImage.h"
#import "FBAlbum.h"
#import "FBUser.h"
#import "FBTag.h"
#import "FacebookBridge.h"
#import "Alerter.h"

#import "NSNumber+FBAdditions.h"


static ApertureBridge *uniqueInstance = nil;

@implementation ApertureBridge

@synthesize context;
@synthesize apContext;
@synthesize exportManager;

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
			
			apContext = [[ApertureContext alloc] init];
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (ApertureBridge *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- (void)saveTagData:(FBImage *)image {
	
	if(image.tags != nil) {
		NSMutableArray *tags = [NSMutableArray array];
		
		for(FBTag *tag in image.tags) {
			NSMutableDictionary *d = [NSMutableDictionary dictionary];
			[d setValue:tag.user.name forKey:@"name"];
			[d setValue:tag.user.uid forKey:@"uid"];
			[d setValue:tag.x forKey:@"xPercent"];
			[d setValue:tag.y forKey:@"yPercent"];
			
			[tags addObject:d];
		}
		
		NSMutableData* data = [NSMutableData data];
		NSKeyedArchiver* archiver = [[[NSKeyedArchiver alloc] initForWritingWithMutableData:data] autorelease];
		NSString* xmlString = nil;
		
		[archiver setOutputFormat:NSPropertyListXMLFormat_v1_0];
		[archiver encodeObject: tags forKey:@"tags"];
		[archiver finishEncoding];
		xmlString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
		
		NSDictionary *metadata = [NSDictionary dictionaryWithObjectsAndKeys: xmlString, kFBTags, nil];

		[exportManager addCustomMetadataKeyValues:metadata toImageAtIndex:image.index.intValue];
	}
}

- (void)savePid:(FBImage *)image {
	
	if(image != nil) {
		
		NSDictionary *metadata = [NSDictionary dictionaryWithObjectsAndKeys: image.pid, kFBPid, nil];
		
		[exportManager addCustomMetadataKeyValues:metadata toImageAtIndex:image.index.intValue];
	}
}

@end

@implementation ApertureBridge (Threads)

- (void)loadPhotos {
	if([exportManager imageCount] > [FBAlbum maxSize])
		[Alerter showMaxPhotosAlert];
	
	[self performSelectorOnMainThread:@selector(loadPhotosThread) withObject:nil waitUntilDone:NO];
}

- (void)loadPhotosThread {
	int i = 0;
	
	self.apContext.state = [ApertureLoadingAlbums instance];
	
	for(i=0;i < [exportManager imageCount];i++) {
		NSDictionary *props = [exportManager propertiesWithoutThumbnailForImageAtIndex:i];

		[FBImage imageFactory:[NSNumber numberWithInt:i] 
			   defaultCaption:[[props valueForKey: kExportKeyIPTCProperties] valueForKey:@"Caption/Abstract"]
				  versionName:[props valueForKey:kExportKeyVersionName]
					thumbnail:[exportManager thumbnailForImageAtIndex:i size:kExportThumbnailSizeMini]
	   inManagedObjectContext:context];
	}
	
	self.apContext.state = [ApertureLoadedAlbums instance];
	
	[[FacebookBridge instance] performSelectorOnMainThread:@selector(login:) withObject:NO waitUntilDone:NO];
}

- (void)loadMetadata {
	[self performSelectorOnMainThread:@selector(loadMetadataThread) withObject:nil waitUntilDone:NO];
}

- (void)loadMetadataThread {
	int i = 0;
	
	// Maybe have a loading tags state?
	
	for(i=0;i < [exportManager imageCount];i++) {
		NSDictionary *props = [exportManager propertiesWithoutThumbnailForImageAtIndex:i];		
		
		NSMutableArray *tags = nil;
		NSString *serializedTags = [[props valueForKey: kExportKeyCustomProperties] valueForKey:kFBTags];
		
		FBImage *image = nil;

		if(serializedTags != nil) {
			NSData *data = [serializedTags dataUsingEncoding: NSASCIIStringEncoding];
			NSKeyedUnarchiver *unarchiver = [[[NSKeyedUnarchiver alloc] initForReadingWithData:data] autorelease];
			
			tags = (NSMutableArray *)[unarchiver decodeObjectForKey:@"tags"];
			[unarchiver finishDecoding];
			
			image = [FBImage imageAtIndex:[NSNumber numberWithInt:i] inManagedObjectContext:context];
			
			for(NSDictionary *tag in tags) {
				FBUser *u = [FBUser userByID:[tag valueForKey:@"uid"] inManagedObjectContext:context];
				if(u == nil)
					u = [[FBUser mainUser:[FacebookBridge instance].context] friendFactory:@""
																					  name:[tag valueForKey:@"name"]
																					 index:[NSNumber numberWithInt:0]
																	inManagedObjectContext:[FacebookBridge instance].context];
				
				[image addTag:u 
					 xPercent:[tag valueForKey:@"xPercent"]
					 yPercent:[tag valueForKey:@"yPercent"]
	   inManagedObjectContext:[FacebookBridge instance].context];
			} // End tag loop
		} // End if serialized tag
		
		NSString *pid = [[props valueForKey: kExportKeyCustomProperties] valueForKey:kFBPid];
		
		if(pid != nil) {
			if(image == nil)
				image = [FBImage imageAtIndex:[NSNumber numberWithInt:i] inManagedObjectContext:context];

			image.pid = pid;
		}
	}
	
	[[FacebookBridge instance] initPidRequest];
}


@end