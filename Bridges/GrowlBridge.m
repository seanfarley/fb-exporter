//
//  GrowlBridge.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/17/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "GrowlBridge.h"

NSString *growlInfoNotification = @"Info Notification";
static GrowlBridge *uniqueInstance = nil;

@implementation GrowlBridge

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;			
		}
	}

	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }
		
+ (GrowlBridge *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}
		
		

- (NSDictionary *)registrationDictionaryForGrowl {
	// For this application, only one notification is registered
	NSArray* defaultNotifications = [NSArray arrayWithObjects:growlInfoNotification, nil];
	NSArray* allNotifications = [NSArray arrayWithObjects:growlInfoNotification, nil];
	
	NSDictionary* growlRegistration = [NSDictionary dictionaryWithObjectsAndKeys: 
									   defaultNotifications, GROWL_NOTIFICATIONS_DEFAULT,
									   allNotifications, GROWL_NOTIFICATIONS_ALL, nil];
	
	return growlRegistration;
}

- (void) growlNotificationWasClicked:(id)clickContext {
	NSURL *url=[NSURL URLWithString:clickContext];
	[[NSWorkspace sharedWorkspace] openURL:url];
}

+(void)notify:(NSString *)title message:(NSString *)message {
	[GrowlBridge notify:title message:message icon:nil clickContext:nil];
}

+(void)notify:(NSString *)title message:(NSString *)message clickContext:(id)context {
	[GrowlBridge notify:title message:message icon:nil clickContext:context];
}

+(void)notify:(NSString *)title message:(NSString *)message icon:(NSImage *)image {
	[GrowlBridge notify:title message:message icon:image clickContext:nil];
}

+(void)notify:(NSString *)title message:(NSString *)message icon:(NSImage *)image clickContext:(id)context {
	NSData *imageData = nil;
	
	if(image != nil) {		
		imageData = [NSBitmapImageRep representationOfImageRepsInArray:[[image representations] retain]
																	usingType:NSJPEGFileType
																   properties:[NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1.0]
																										  forKey:NSImageCompressionFactor]];
	}
	
	
	[GrowlApplicationBridge notifyWithTitle:title
								description:message
						   notificationName:growlInfoNotification
								   iconData:imageData
								   priority:0
								   isSticky:NO 
							   clickContext:context];
	
}

@end
