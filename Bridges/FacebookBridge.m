//
//  FacebookBridge.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/21/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FacebookBridge.h"
#import "GrowlBridge.h"

#import "NSNumber+FBAdditions.h"
#import "FBUser.h"
#import "FBAlbum.h"
#import "FBImage.h"
#import "FBTag.h"

#import "FacebookGUI.h"

#import "FacebookContext.h"

#import "Alerter.h"
#import "ApertureBridge.h"
#import "FacebookExporter.h"
#import "NSAttributedString+FBAdditions.h"

//Quite the hack
#ifdef MKABEFOOK
	#import "MKAbeFookImplementation.h"
#elif FBCOCOA
	#import "FBCocoaImplementation.h"
#endif


static FacebookBridge *uniqueInstance = nil;

@implementation FacebookBridge

@synthesize api;
@synthesize context;
@synthesize fbContext;

@synthesize plugin;

@synthesize doneResize=_doneResize, doneAlbum=_doneAlbum;

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
			

			api = [[ConcreteFacebookAPI facebookWithAPI:kFBExportAPI delegate:self selector:@selector(userLoginSuccessful:)] retain];

			fbContext = [[FacebookContext alloc] init];
			
			requestImageQueue = [[ConcreteFacebookRequestQueueAPI alloc] init];
			requestImageQueue.delegate = self;
			requestImageQueue.api = api;
			requestImageQueue.currentlySendingSelector = @selector(currentlyUploadingImage:);
			requestImageQueue.lastRequestResponseSelector = @selector(lastImageUploaded:);
			requestImageQueue.allRequestsFinishedSelector = @selector(allImagesUploaded);
			requestImageQueue.debugName = @"image";
			
			requestTagQueue = [[ConcreteFacebookRequestQueueAPI alloc] init];
			requestTagQueue.delegate = self;
			requestTagQueue.api = api;
			requestTagQueue.currentlySendingSelector = @selector(currentlyUploadingTag:);
			requestTagQueue.lastRequestResponseSelector = @selector(lastTagUploaded:);
			requestTagQueue.allRequestsFinishedSelector = @selector(allTagsUploaded);
			requestTagQueue.debugName = @"tag";
			
			pidRequest = nil;
			
			_cancelRequests = NO;
			_doneAlbum = NO;
			_doneResize = NO;
			_doneUploadingImages = NO;
			_doneUploadingTags = NO;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (void)cleanup
{
	[userInfoRequest release];
	[albumsRequest release];
	[friendsRequest release];
	[permissionRequest release];
	[pidRequest release];
	[requestImageQueue release];
	[requestTagQueue release];
	
	uniqueInstance = nil;
}

- (id)autorelease { return self; }

+ (FacebookBridge *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- initRequests {
	
	// Clear all the user info
	[FBUser clearAllData:context];
	
	// User Info Request
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	userInfoRequest = [[ConcreteFacebookRequestAPI alloc] init];
	userInfoRequest.delegate = self;
	userInfoRequest.selector = @selector(gotUserInfo:);
	userInfoRequest.api = api;
	
	[parameters setValue:@"facebook.users.getInfo" forKey:@"method"];
	[parameters setValue:[api uid] forKey:@"uids"];
	[parameters setValue:@"name" forKey:@"fields"];
	
	[userInfoRequest setParameters:parameters];
	
//	[parameters release];
	
	// Albums Request
	parameters = [[NSMutableDictionary alloc] init];
	albumsRequest = [[ConcreteFacebookRequestAPI alloc] init];
	
	//set up request object
	albumsRequest.delegate = self;
	albumsRequest.selector = @selector(gotAlbumList:);
	albumsRequest.api = api;
	
	//set up parameters for the request
	[parameters setValue:@"facebook.photos.getAlbums" forKey:@"method"];
	[parameters setValue:[api uid] forKey:@"uid"];
	
	//add paramters to request
	[albumsRequest setParameters:parameters];
	
//	[parameters release];
	
	/// Friends Request
	parameters = [[NSMutableDictionary alloc] init];
	friendsRequest = [[ConcreteFacebookRequestAPI alloc] init];
	
	//set up request object
	friendsRequest.delegate = self;
	friendsRequest.selector = @selector(gotFriendList:);
	friendsRequest.api = api;
	
	//set up parameters for the request
	[parameters setValue:@"facebook.fql.query" forKey:@"method"];
	[parameters setValue:[NSString stringWithFormat:@"SELECT uid, name FROM user WHERE uid in (SELECT uid2 FROM friend WHERE uid1=%@) ORDER BY name", [api uid]] forKey:@"query"];
	
	//add paramters to request
	[friendsRequest setParameters:parameters];
	
//	[parameters release];
	
	
	// New Album Request
	newAlbumRequest = [[ConcreteFacebookRequestAPI alloc] init];
	
	newAlbumRequest.delegate = self;
	newAlbumRequest.selector = @selector(gotCreatedAlbum:);
	newAlbumRequest.api = api;
	
// Weird GC bug?
//	[parameters release];
	
	// Publish_stream permission request
	parameters = [[NSMutableDictionary alloc] init];
	permissionRequest = [[ConcreteFacebookRequestAPI alloc] init];
	permissionRequest.delegate = self;
	permissionRequest.selector = @selector(gotPermissionResponse:);
	permissionRequest.api = api;
	
	[parameters setValue:@"facebook.fql.query" forKey:@"method"];
	[parameters setValue:[NSString stringWithFormat:@"SELECT publish_stream FROM permissions WHERE uid=%@", [api uid]] forKey:@"query"];
	
	[permissionRequest setParameters:parameters];
	
	//	[parameters release];
	
	return self;
}

- initPidRequest {
	
	// User Info Request
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	pidRequest = [[ConcreteFacebookRequestAPI alloc] init];
	[pidRequest setDelegate:self];
	[pidRequest setSelector:@selector(gotPidResponse:)];
	
	[parameters setValue:@"facebook.fql.query" forKey:@"method"];
	[parameters setValue:[NSString stringWithFormat:@"SELECT pid FROM photo WHERE pid IN (%@)", [[FBImage getAllPids:context] componentsJoinedByString:@","]] forKey:@"query"];
	
	[pidRequest setParameters:parameters];
	
	//	[parameters release];
	
	return self;
}

- cancelAllRequests {
	_cancelRequests = YES;
	[userInfoRequest cancelRequest];
	[albumsRequest cancelRequest];
	[friendsRequest cancelRequest];
	[permissionRequest cancelRequest];
	[pidRequest cancelRequest];
	[requestImageQueue cancelRequestQueue];
	[requestTagQueue cancelRequestQueue];
	return self;
}

#pragma mark Log in / User Info

- (BOOL)userLoggedIn {
	return [api userLoggedIn];
}

- login:(BOOL)shouldPrompt {

	if ([api userLoggedIn]) {
		[self userLoginSuccessful:nil];
		return self;
	}

	if(shouldPrompt) {
		NSWindow *w = [api login];
		
		[NSApp beginSheet: w
		   modalForWindow: [FacebookGUI instance].window 
			modalDelegate: nil 
		   didEndSelector: nil
			  contextInfo: nil];
		
		fbContext.state = [FBAuthenticating instance];
	}
	
	if(![[FacebookGUI instance].signInButton isEnabled])
		[[FacebookGUI instance].signInButton setEnabled:YES];
	
	return self;
}

- logout {
	[api logout];
	
	// Clear all the user info
	[FBUser clearAllData:context];
	
	return self;
}
	

- userLoginSuccessful:(NSError*)err {
	_cancelRequests = NO;
	
	[self initRequests];
	
	fbContext.state = [FBRetrievingUserInfo instance];
	
	[userInfoRequest sendRequest];
	
	return self;
}

- userLoginFailed:(NSError*)err {
	[Log errorLog:@"userLoginFailed needs to print error message: %@", err];
	FacebookState *s = [fbContext.state handleAction];
	if(s != nil)
		fbContext.state = s;
	
	return self;
}

- gotUserInfo:(NSDictionary *)info {
	if(info != nil) {
		[FBUser userFactory:[NSString stringWithFormat:@"%@",[api uid]]
								   name:[NSString stringWithFormat:@"%@ (me)",[info valueForKey:@"name"]]
								  index:[NSNumber numberWithInt:0]
				 inManagedObjectContext:context];
		
		[self requestAlbums];
	} else {
		fbContext.state = [FBLoggedOut instance];
	}
	
	return self;
}

- facebookErrorResponseReceived:(id)error {

	FacebookState *s = [fbContext.state handleAction];
	if(s != nil)
		fbContext.state = s;
	
	return self;
}

#pragma mark Albums

- requestAlbums {
	fbContext.state = [FBRetrievingAlbums instance];

	[albumsRequest sendRequest];

	return self;
}

- gotAlbumList:(NSArray *)albums {

	FBUser *u = [FBUser mainUser:context];
	if(albums != nil) {
		for(NSDictionary *album in albums) {
			FBAlbum *a = [FBAlbum albumFactory:[album objectForKey:@"aid"]
							 name:[album objectForKey:@"name"]
						  created:[[album objectForKey:@"created"] stringValue]
							 size:[album objectForKey:@"size"]
							 user:u inManagedObjectContext:self.context];
			a.link = [album objectForKey:@"link"];
		}
	} else {
		[Alerter showAlbumConnectionAlert];
	}
	
	[context processPendingChanges];
	
	FBAlbum *a = [FBAlbum albumByName:[FBAlbum defaultAlbum:context].name inManagedObjectContext:context];
	
	if(a != nil) {
		[FacebookGUI instance].useExistingAlbum = YES;
		[[FacebookGUI instance].albumArray setSelectedObjects:[NSArray arrayWithObject:a]];
	} else {
		[FacebookGUI instance].useExistingAlbum = NO;
	}

	[self requestFriends];
	
	return self;
}

#pragma mark Friends

- requestFriends {
	fbContext.state = [FBRetrievingFriends instance];

	[friendsRequest sendRequest];

	return self;
}

- gotFriendList:(NSArray *)friends {
	FBUser *u = [FBUser mainUser:context];

	if(friends != nil) {
		u.index = [NSNumber numberWithInt:(-1 * [friends count])];
		int i=1;
		for(NSDictionary *friend in friends) {
			[u friendFactory:[[friend objectForKey:@"uid"] stringValue]
						name:[friend objectForKey:@"name"]
					   index:[NSNumber numberWithInt:i++]
	  inManagedObjectContext:context];
		}
	} else {
		[Alerter showFriendsConnectionAlert];
	}
	
	[[ApertureBridge instance] loadMetadata];
	
	[self requestPermission];
	
	return self;
}

#pragma mark Images

- currentlyUploadingImage:(NSDictionary *)info {
	currentImage = [FBImage imageAtIndex:[info valueForKey:@"current"] inManagedObjectContext:context];
	currentTime = [[NSDate date] retain];

	[plugin lockProgress];
	[[plugin progress]->message release];
	[plugin progress]->currentValue++;
	[plugin progress]->message = [[NSString stringWithFormat:@"Step 2: Uploading Image %d of %d...",  [plugin progress]->currentValue, [plugin progress]->totalValue] retain];
	[plugin unlockProgress];
	
	return self;
}

- lastImageUploaded:(NSDictionary *)info {

	currentImage.uploadTime = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSinceDate:currentTime]];

	[GrowlBridge notify:[NSString stringWithFormat:@"Uploaded %@",currentImage.name]
				message:[NSString stringWithFormat:@"Time: %@", [currentImage.uploadTime timeValue]]
				   icon:[currentImage.thumbnail retain]];
	
	currentImage.pid = [info valueForKey:@"pid"];
	
	// TODO: Need to handle errors
	//if([image.shouldUpload boolValue])
	[self queueTags:currentImage];

	[requestTagQueue startRequestQueue];
	
	return self;
}

- allImagesUploaded {
	if(_cancelRequests)
		return self;
	
//	[plugin lockProgress];
//	[plugin progress]->indeterminateProgress = YES;
//	[plugin progress]->currentValue = 0;
//	[plugin progress]->totalValue = [requestTagQueue count];
//	[plugin progress]->message = [@"Beginning Tagging..." retain];
//	[plugin unlockProgress];
	
	_doneUploadingImages = YES;
	[self finishUpload];
	
	return self;
}

- queueImage:(FBImage *)image {
	
	FacebookRequestAPI* request = [[ConcreteFacebookRequestAPI alloc] init];
	
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	[parameters setValue:@"facebook.photos.upload" forKey:@"method"];
	
	if(image.caption != nil)
		[parameters setValue:image.caption forKey:@"caption"];
	
	NSImage *dasImage = [[[NSImage alloc] initByReferencingFile:image.path] retain];
	
	[parameters setObject:dasImage forKey:@"image"];

	// TODO: make a better pattern to access this
	FBAlbum *a = [[[FacebookGUI instance].albumArray selectedObjects] lastObject];

	[parameters setValue:a.aid forKey:@"aid"];
	
	[request setParameters:parameters];
	[request setScheduleLoopIsModal:NO]; // Need to make it in the current loop because the dialog box closes
	[requestImageQueue addRequest:request];
	
//	[parameters release];
//	[request release];
	
	return self;
}

- startUploadingImages {
	
	if(_doneAlbum && _doneResize) {
		// Fetch the images in order
		NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBImage" inManagedObjectContext:context];
		NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
		fetch.entity = entity;
		fetch.sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
		
		NSArray *images = [context executeFetchRequest:fetch error:nil];

		// Queue the images
		for(FBImage *i in images) {
			if([i.shouldUpload boolValue]) [[FacebookBridge instance] queueImage:i];
		}
		
		[requestImageQueue startRequestQueue];
	}
	
	return self;
}

#pragma mark Tagging

- currentlyUploadingTag:(NSDictionary *)info {
//	currentImage = [FBImage imageAtIndex:[info valueForKey:@"current"] inManagedObjectContext:context];
//	
//	[plugin lockProgress];
//	[[plugin progress]->message release];
//	[plugin progress]->currentValue++;
//	[plugin progress]->message = [[NSString stringWithFormat:@"Step 3: Uploading tag %d of %d...",  [plugin progress]->currentValue, [plugin progress]->totalValue] retain];
//	[plugin unlockProgress];
	
	return self;
}

- lastTagUploaded:(NSDictionary *)info {	
	return self;
}

- allTagsUploaded {
	_doneUploadingTags = YES;
	[self finishUpload];
	
	return self;
}

- finishUpload {
	if(_doneUploadingTags && _doneUploadingImages)
		[plugin exportManagerDidFinishUpload];
	
	return self;
}

- queueTags:(FBImage *)image {
	
	for(FBTag *tag in image.tags) {
		// Cheap assignment, even though called multiple times
		_doneUploadingTags = NO;
		
		ConcreteFacebookRequestAPI *request = [[ConcreteFacebookRequestAPI alloc] init];
		[request setDelegate:self];
		
		NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
		[parameters setValue:@"facebook.photos.addTag" forKey:@"method"];
		
		NSNumber *x = [NSNumber numberWithFloat:[tag.x floatValue]*100];
		NSNumber *y = [NSNumber numberWithFloat:(100-[tag.y floatValue]*100)];
		
		[parameters setValue:tag.image.pid forKey:@"pid"];
		[parameters setValue:tag.user.name forKey:@"tag_text"];
		if(![tag.user.uid isEqualToString: @""])
			[parameters setValue:tag.user.uid forKey:@"tag_uid"];
		[parameters setValue:[x stringValue] forKey:@"x"];
		[parameters setValue:[y stringValue] forKey:@"y"];
		
		[request setScheduleLoopIsModal:NO];
		[request setParameters:parameters];
		[requestTagQueue addRequest:request];
		
		[parameters release];
		[request release];
	}

	return self;
}

- startUploadingTags {
	[requestImageQueue startRequestQueue];
	
	return self;
}

#pragma mark Create Album

- createAlbum:(NSString *)name albumLocation:(NSString *)location albumDescription:(NSString *)description albumVisibility:(NSInteger)tag {
	
	[self shouldBeginExport];
	
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	
	[parameters setValue:@"facebook.photos.createAlbum" forKey:@"method"];
	[parameters setValue:name forKey:@"name"];
	
	if(location != nil)
		[parameters setValue:location forKey:@"location"];
	
	if(description != nil)
		[parameters setValue:description forKey:@"description"];
	
	NSString *visible;
	switch (tag) {
		case 0:
			visible = @"everyone";
			break;
		case 1:
			visible = @"networks";
			break;
		case 2:
			visible = @"friends-of-friends";
			break;
		case 3:
			visible = @"friends";
			break;
		default:
			break;
	}
	
	[parameters setValue:visible forKey:@"visible"];
	
	[newAlbumRequest setScheduleLoopIsModal:NO];
	[newAlbumRequest setParameters:parameters];
	[newAlbumRequest sendRequest];
	[parameters release];
	
	return self;
}

- gotCreatedAlbum:(NSDictionary *)album {
	
	if(album == nil) {		
		[Alerter showCreateAlbumError];
		[plugin._exportManager shouldCancelExport];
		
		return self;
	}
	
	FBAlbum *a = [FBAlbum defaultAlbum:context];
	a.aid = [album valueForKey:@"aid"];
	a.link = [album valueForKey:@"link"];

	[context processPendingChanges];
	
	[[FacebookGUI instance].albumArray setSelectedObjects:[NSArray arrayWithObject:a]];

//	[self shouldBeginExport];
	_doneAlbum = YES;
	
	[Log infoLog:@"Finished creating album"];

	[GrowlBridge notify:[NSString stringWithFormat:@"Created %@",a.name]
			message:@"Album created"
			   icon:nil];
	
	[self startUploadingImages];
	
	return self;
}

#pragma mark Permissions

- requestPermission {
	fbContext.state = [FBRetrievingPermission instance];
	
	[permissionRequest sendRequest];
	
	return self;
}

- gotPermissionResponse:(NSDictionary *)permissions {
	if(permissions != nil) {		
		[FacebookGUI instance].hasPublish = [[permissions objectForKey:@"publish_stream"] boolValue];
		
		
		
		fbContext.state = [FBLoggedIn instance];
	} else {
		[Alerter showConnectionError];
	}
	
	return self;
}

- revokePermission {
	// Hack, need to make into FBState
	[[FacebookGUI instance].permissionButton setEnabled:NO];
	
	NSMutableDictionary *parameters = [[NSMutableDictionary alloc] init];
	FacebookRequestAPI *revokeRequest = [[ConcreteFacebookRequestAPI alloc] init];
	revokeRequest.delegate = self;
	revokeRequest.api = api;
	revokeRequest.selector = @selector(requestPermission);
	
	[parameters setValue:@"facebook.auth.revokeExtendedPermission" forKey:@"method"];
	[parameters setValue:@"publish_stream" forKey:@"perm"];
	
	[revokeRequest setParameters:parameters];
	
	[revokeRequest sendRequest];
	
	return self;
}

- handlePublishPermission:(BOOL)grant {
	
	if(grant) {
		[[FacebookGUI instance] settingsClosed:nil];

		// TODO: Fix for MK framework
		NSWindow *w = [api grantExtendedPermission:@"publish_stream" forSheet:YES delegate:self selector:@selector(gotPermissionResponse:)];

		[NSApp beginSheet: w
		   modalForWindow: [FacebookGUI instance].window 
			modalDelegate: self 
		   didEndSelector: @selector(requestPermission)
			  contextInfo: nil];
	} else {
		[self revokePermission];
	}

	return self;
}

#pragma mark PIDs

- requestPids {
	fbContext.state = [FBRetrievingPids instance];
	
	[pidRequest sendRequest];
	
	return self;
}

- gotPidResponse:(NSArray *)pids {
	fbContext.state = [FBLoggedIn instance];
	
	// Might need to error check
	
	if(pids != nil) {
		for(NSDictionary *pid in pids) {
			FBImage *image = [FBImage imageByPid:[pid objectForKey:@"pid"] inManagedObjectContext:context];
			image.shouldUpload = [NSNumber numberWithBool:NO];
		}
	} else {
		[Log errorLog:@"Need to print out gotPidResponse error"];
	}
	
	[plugin._exportManager shouldBeginExport];
	
	return self;
}

// TODO: Check image ratio
- shouldBeginExport {
	
	if([FacebookGUI instance].shouldTryOnlyTag) {
		[self requestPids];
	} else {
		[plugin._exportManager shouldBeginExport];
	}
	
	return self;
}

// YES if there is an error
- (BOOL)checkErrorBeforeExport {

	if(![[FacebookBridge instance] userLoggedIn]) {
		[Alerter showAlert:@"Facebook Error"
			   withMessage:@"You are currently not logged into Facebook. Please log in to begin export."];
		return YES;
	}
	
	NSUInteger serverCount = 0;
	// TODO: make a better pattern to access this
	if([FacebookGUI instance].useExistingAlbum) {
		FBAlbum *a = [[[FacebookGUI instance].albumArray selectedObjects] lastObject];
		serverCount = [[a size] unsignedIntegerValue];
	}
	NSUInteger totalCount = [[[ApertureBridge instance] exportManager] imageCount] + serverCount;
	
	if(totalCount > [FBAlbum maxSize]) {
		[Alerter showAlert:@"Facebook Error"
			   withMessage:@"You are trying to upload too many photos into an album that doesn't have enough space. The current limit is 200 photos per album."];
		return YES;
	}
	
	return NO;
}


@end
