//
//  User.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/22/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBUser.h"

@implementation FBUser

@dynamic uid;
@dynamic name;
@dynamic index;
@dynamic albums;
@dynamic friends;
@dynamic user;
@dynamic tags;

- (NSArray *)sortByName {
	if( _sortByName == nil ) {
		_sortByName = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"name" ascending:YES]];
	}
	
	return _sortByName;
}

- (NSArray *)sortByIndex {
	if( _sortByIndex == nil ) {
		_sortByIndex = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
	}
	
	return _sortByIndex;
}
	

- (FBUser *)friendFactory:(NSString *)uid name:(NSString *)name index:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context {
	FBUser *u = [FBUser userFactory:uid name:name index:index inManagedObjectContext:context];
	u.user = self;
	
	return u;
}

+ (FBUser *)mainUser:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBUser" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"user == nil"];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	// Execute the fetch, and get the resulting element from the array
	FBUser *u = [[context executeFetchRequest:fetch error:nil] lastObject];
	
	return u;
}

+ (FBUser *)userFactory:(NSString *)uid name:(NSString *)name index:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context {

	FBUser *u = [NSEntityDescription insertNewObjectForEntityForName:@"FBUser" inManagedObjectContext:context];
	u.uid = uid;
	u.name = name;
	u.index = index;
	
	return u;
}

+ (FBUser *)userByName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBUser" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@", name];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	fetch.sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
	
	FBUser *u = [[context executeFetchRequest:fetch error:nil] lastObject];
	return u;
}

+ (FBUser *)userByID:(NSString *)uid inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBUser" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"uid == %@", uid];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	fetch.sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
	
	FBUser *u = [[context executeFetchRequest:fetch error:nil] lastObject];
	return u;
}

+ (void)clearAllData:(NSManagedObjectContext *)context {
	FBUser *u = [FBUser mainUser:context];
	 if(u != nil)
		 [context deleteObject:u];
}

+ (NSNumber *)currentIndex:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBUser" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"index < 0"];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	fetch.sortDescriptors = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
	
	FBUser *u = [[context executeFetchRequest:fetch error:nil] lastObject];

	return u.index;
}

@end
