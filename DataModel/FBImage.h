//
//  FBImage.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/24/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FBUser;
@class FBTag;

@interface FBImage : NSManagedObject {
	NSArray *_sortByIndex;
}

@property (readwrite, copy) NSNumber *index;
@property (readwrite, copy) NSString *caption;
@property (readwrite, copy) NSString *name;
@property (readwrite, copy) NSString *path;
@property (readwrite, copy) NSString *pid;
@property (readwrite, copy) NSNumber *shouldUpload;
@property (readwrite, copy) NSNumber *uploadTime;
@property (readwrite, copy) NSImage *thumbnail;
@property (readwrite, retain) NSSet *tags;

@property (readonly) NSArray *sortByIndex;

- (FBTag *)getTaggedUser:(FBUser *)user inManagedObjectContext:(NSManagedObjectContext *)context;
- (FBTag *)addTag:(FBUser *)u xPercent:(NSNumber *)x yPercent:(NSNumber *)y inManagedObjectContext:(NSManagedObjectContext *)context;

+ (FBImage *)imageFactory:(NSNumber *)index defaultCaption:(NSString *)caption versionName:(NSString *)name thumbnail:(NSImage *)image inManagedObjectContext:(NSManagedObjectContext *)context;
+ (FBImage *)imageAtIndex:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context;
+ (FBImage *)imageByPid:(NSString *)pid inManagedObjectContext:(NSManagedObjectContext *)context;
+ (NSArray *)getAllFBImagesWithPids:(NSManagedObjectContext *)context;
+ (NSArray *)getAllPids:(NSManagedObjectContext *)context;

@end
