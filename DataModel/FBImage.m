//
//  FBImage.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/24/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBImage.h"
#import "FBUser.h"
#import "FBTag.h"
#import "ApertureBridge.h"


@implementation FBImage

@dynamic index;
@dynamic caption;
@dynamic name;
@dynamic path;
@dynamic pid;
@dynamic shouldUpload;
@dynamic uploadTime;
@dynamic thumbnail;
@dynamic tags;

- (void)setPid:(NSString *)newPid {
	[self willChangeValueForKey:@"pid"];

	[self setPrimitiveValue:newPid forKey:@"pid"];

    [self didChangeValueForKey:@"pid"];
	
	[[ApertureBridge instance] savePid:self];
}

- (NSArray *)sortByIndex {
	if( _sortByIndex == nil ) {
		_sortByIndex = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"index" ascending:YES]];
	}
	
	return _sortByIndex;
}


- (FBTag *)getTaggedUser:(FBUser *)user inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBTag" inManagedObjectContext:context];
	NSPredicate *predicate0 = [NSPredicate predicateWithFormat:@"image == %@", self];
	NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"user == %@", user];
	NSPredicate *andPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate0,predicate1,nil]];

	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = andPredicate;
	
	return [[context executeFetchRequest:fetch error:nil] lastObject];
}

- (FBTag *)addTag:(FBUser *)u xPercent:(NSNumber *)x yPercent:(NSNumber *)y inManagedObjectContext:(NSManagedObjectContext *)context {
	
	// Only change the index if this is not the main user
	if(u.user != nil) {
		NSNumber *n = [FBUser currentIndex:context];
		u.index = [NSNumber numberWithInt:[n intValue]+1];
	}
	
	FBTag *tag = [self getTaggedUser:u inManagedObjectContext:context];
	
	if(tag == nil)
	    tag = [NSEntityDescription insertNewObjectForEntityForName:@"FBTag" inManagedObjectContext:context];
	
	tag.x = x;
	tag.y = y;
	
	tag.user = u;
	tag.image = self;
	
	return tag;
}

+ (FBImage *)imageFactory:(NSNumber *)index defaultCaption:(NSString *)caption versionName:(NSString *)name thumbnail:(NSImage *)image inManagedObjectContext:(NSManagedObjectContext *)context {
	FBImage *f = [NSEntityDescription insertNewObjectForEntityForName:@"FBImage" inManagedObjectContext:context];
	f.index = index;
	f.caption = caption;
	f.name = name;
	f.thumbnail = image;
	return f;
}

+ (FBImage *)imageAtIndex:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBImage" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"index == %@", index];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	return [[context executeFetchRequest:fetch error:nil] lastObject];
}

+ (FBImage *)imageByPid:(NSString *)pid inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBImage" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pid == %@", pid];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	return [[context executeFetchRequest:fetch error:nil] lastObject];
}

+ (NSArray *)getAllFBImagesWithPids:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBImage" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"pid != '' AND pid != nil", index];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	return [context executeFetchRequest:fetch error:nil];
}

+ (NSArray *)getAllPids:(NSManagedObjectContext *)context {
	NSArray *images = [FBImage getAllFBImagesWithPids:context];
	NSMutableArray *pids = [NSMutableArray array];
	
	for(FBImage *image in images) {
		[pids addObject:image.pid];
	}
	
	return pids;
}

@end
