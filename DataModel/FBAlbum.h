//
//  Album.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/22/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FBUser;

@interface FBAlbum : NSManagedObject {
	NSArray *_sortByCreated;
}

@property (readwrite, copy) NSString *aid;
@property (readwrite, copy) NSNumber *size;
@property (readwrite, copy) NSString *name;
@property (readwrite, copy) NSString *link;
@property (readwrite, copy) NSString *created;
@property (readwrite, copy) NSString *fbDescription;
@property (readwrite, copy) NSString *location;
@property (readwrite, retain) FBUser *user;
@property (readonly) NSString *displayName;

@property (readonly) NSArray *sortByCreated;

+ (int)maxSize;
+ (FBAlbum *)defaultAlbum:(NSManagedObjectContext *)context;
+ (FBAlbum *)albumFactory:(NSString *)aid name:(NSString *)name created:(NSString *)created size:(NSNumber *)size user:(FBUser *)user inManagedObjectContext:(NSManagedObjectContext *)context;
+ (FBAlbum *)albumByName:(NSString *)albumName inManagedObjectContext:(NSManagedObjectContext *)context;

@end
