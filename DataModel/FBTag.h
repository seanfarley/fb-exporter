//
//  FBTags.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/26/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FBImage;
@class FBUser;

@interface FBTag : NSManagedObject {

}

@property (readwrite, copy) NSNumber *x;
@property (readwrite, copy) NSNumber *y;
@property (readwrite, retain) FBImage *image;
@property (readwrite, retain) FBUser *user;

@end
