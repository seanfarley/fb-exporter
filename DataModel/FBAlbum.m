//
//  Album.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/22/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBAlbum.h"
#import "FBUser.h"


@implementation FBAlbum

@dynamic aid;
@dynamic size;
@dynamic name;
@dynamic link;
@dynamic created;
@dynamic fbDescription;
@dynamic location;
@dynamic user;

- (NSArray *)sortByCreated {
	if( _sortByCreated == nil ) {
		_sortByCreated = [NSArray arrayWithObject:[[NSSortDescriptor alloc] initWithKey:@"created" ascending:NO]];
	}
	
	return _sortByCreated;
}

- (NSString*)displayName {
	return [NSString stringWithFormat:@"%@ (%@)", self.name, self.size];
}

+ (int)maxSize {
	return 200;
}

+ (FBAlbum *)defaultAlbum:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBAlbum" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"created == nil"];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	FBAlbum *a = [[context executeFetchRequest:fetch error:nil] lastObject];
	return a;
}
	

+ (FBAlbum *)albumFactory:(NSString *)aid name:(NSString *)name created:(NSString *)created size:(NSNumber *)size user:(FBUser *)user inManagedObjectContext:(NSManagedObjectContext *)context {
	
	FBAlbum *a = [NSEntityDescription insertNewObjectForEntityForName:@"FBAlbum" inManagedObjectContext:context];
	a.aid = aid;
	a.name = name;
	a.created = created;
	a.size = size;
	a.user = user;
	
	return a;
}

+ (FBAlbum *)albumByName:(NSString *)albumName inManagedObjectContext:(NSManagedObjectContext *)context {
	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FBAlbum" inManagedObjectContext:context];
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ AND created != nil", albumName];
	NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
	fetch.entity = entity;
	fetch.predicate = predicate;
	
	FBAlbum *a = [[context executeFetchRequest:fetch error:nil] lastObject];
	return a;
}

@end
