//
//  FBTags.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/26/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBTag.h"
#import "FBUser.h"
#import "FBImage.h"


@implementation FBTag

@dynamic x;
@dynamic y;
@dynamic image;
@dynamic user;

@end
