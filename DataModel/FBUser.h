//
//  User.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/22/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FBUser : NSManagedObject {
	NSArray *_sortByName;
	NSArray *_sortByIndex;
}

@property (readwrite, copy) NSString *uid;
@property (readwrite, copy) NSString *name;
@property (readwrite, copy) NSNumber *index;
@property (readwrite, retain) NSSet *albums;
@property (readwrite, retain) NSSet *friends;
@property (readwrite, retain) NSSet *tags;
@property (readwrite, retain) FBUser *user;

@property (readonly) NSArray *sortByName;
@property (readonly) NSArray *sortByIndex;

- (FBUser *)friendFactory:(NSString *)uid name:(NSString *)name index:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context;

+ (FBUser *)mainUser:(NSManagedObjectContext *)context;
+ (FBUser *)userFactory:(NSString *)uid name:(NSString *)name index:(NSNumber *)index inManagedObjectContext:(NSManagedObjectContext *)context;
+ (FBUser *)userByName:(NSString *)name inManagedObjectContext:(NSManagedObjectContext *)context;
+ (FBUser *)userByID:(NSString *)uid inManagedObjectContext:(NSManagedObjectContext *)context;
+ (void)clearAllData:(NSManagedObjectContext *)context;
+ (NSNumber *)currentIndex:(NSManagedObjectContext *)context;

@end
