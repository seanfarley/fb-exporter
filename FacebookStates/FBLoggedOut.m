//
//  FBLoggedOut.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBLoggedOut.h"
#import "FacebookGUI.h"
#import "FacebookBridge.h"
#import "FBUser.h"


static FBLoggedOut *uniqueInstance = nil;

@implementation FBLoggedOut

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (FacebookState *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- handleAction {
	[[FacebookBridge instance] login:YES];
//	[[FacebookBridge instance] performSelectorOnMainThread:@selector(login:) withObject:YES waitUntilDone:YES modes:[NSArray arrayWithObjects:NSModalPanelRunLoopMode, NSEventTrackingRunLoopMode, nil]];
//	[[FacebookBridge instance] performSelectorOnMainThread:@selector(login:) withObject:YES waitUntilDone:NO];
	return nil;
}

- changeState {
	[FBUser clearAllData:[FacebookBridge instance].context];
	
	[FacebookGUI instance].signInButton.title = @"Sign In";
	[[FacebookGUI instance].signInButton setEnabled:YES];
	[[FacebookGUI instance].fbProgress stopAnimation:self];
	[[FacebookGUI instance].fbProgress setHidden:YES];
	[[FacebookGUI instance].fbStatus setTitle:@""];
	[[FacebookGUI instance].fbStatusField setHidden:YES];
	
	[FacebookGUI instance].tagSignInButton.title = @"Sign In";
	[[FacebookGUI instance].tagSignInButton setHidden:NO];
	[[FacebookGUI instance].scrollViewNames setHidden:YES];
	
	[[FacebookGUI instance].permissionButton setEnabled:NO];
	
	return self;
}

@end
