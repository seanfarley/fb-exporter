//
//  FBLoggedIn.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBLoggedIn.h"
#import "FBLoggedOut.h"
#import "FacebookGUI.h"
#import "FacebookBridge.h"


static FBLoggedIn *uniqueInstance = nil;

@implementation FBLoggedIn

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (FacebookState *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- handleAction {
	[[FacebookBridge instance] logout];
	return [FBLoggedOut instance];
}


- changeState {
	[FacebookGUI instance].signInButton.title = @"Sign Out";
	[[FacebookGUI instance].signInButton setEnabled:YES];
	[[FacebookGUI instance].fbProgress stopAnimation:self];
	[[FacebookGUI instance].fbProgress setHidden:YES];
	[[FacebookGUI instance].fbStatus setTitle:@""];
	[[FacebookGUI instance].fbStatusField setHidden:YES];
	
	[FacebookGUI instance].tagSignInButton.title = @"Sign Out";
	[[FacebookGUI instance].tagSignInButton setHidden:YES];
	[[FacebookGUI instance].scrollViewNames setHidden:NO];
	
	[[FacebookGUI instance].permissionButton setEnabled:YES];
	
	return self;
}

@end
