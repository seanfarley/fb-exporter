//
//  FacebookState.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FacebookState.h"
#import "FacebookBridge.h"
#import "FBLoggedOut.h"

@implementation FacebookState

+ (FacebookState *)instance {
	return nil;
}

- handleAction {
	[[FacebookBridge instance] cancelAllRequests];
	return [FBLoggedOut instance];
}

- changeState {	
	return self;
}

@end
