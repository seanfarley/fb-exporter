//
//  FBRetrievingFriends.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/26/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBRetrievingFriends.h"
#import "FacebookGUI.h"
#import "FacebookBridge.h"
#import "FBLoggedOut.h"


static FBRetrievingFriends *uniqueInstance = nil;

@implementation FBRetrievingFriends

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (FacebookState *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- changeState {
	[FacebookGUI instance].signInButton.title = @"Cancel";
	[[FacebookGUI instance].signInButton setEnabled:YES];
	[[FacebookGUI instance].fbProgress setHidden:NO];
	[[FacebookGUI instance].fbProgress startAnimation:self];
	[[FacebookGUI instance].fbStatusField setHidden:NO];
	[[FacebookGUI instance].fbStatus setTitle:@"Retrieving friend list..."];
	
	return self;
}


@end
