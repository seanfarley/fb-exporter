//
//  FacebookState.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FacebookState : NSObject {

}

+ (FacebookState *)instance;

- handleAction;
- changeState;

@end
