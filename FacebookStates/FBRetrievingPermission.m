//
//  FBRetrievingPermission.m
//  FacebookExporter
//
//  Created by Sean Farley on 5/30/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FBRetrievingPermission.h"
#import "FacebookBridge.h"
#import "FacebookGUI.h"
#import "FBLoggedOut.h"

static FBRetrievingPermission *uniqueInstance = nil;

@implementation FBRetrievingPermission

- (id)init {
	Class myClass = [self class];
	@synchronized(myClass) {
		if(uniqueInstance == nil && (self = [super init])) {
			uniqueInstance = self;
		}
	}
	
	return uniqueInstance;
}

+ (id)allocWithZone:(NSZone *)zone {
    @synchronized(self) {
        if (uniqueInstance == nil) {
            return [super allocWithZone:zone];
        }
    }
    return uniqueInstance;
}

- (id)copyWithZone:(NSZone *)zone { return self; }

- (id)retain { return self; }

- (NSUInteger)retainCount { return UINT_MAX; }

- (void)release {}

- (id)autorelease { return self; }

+ (FacebookState *)instance {
	@synchronized( self ) {
		if (uniqueInstance == nil) {
			uniqueInstance = [[self alloc] init];
		}
	}
	
	return uniqueInstance;
}

- changeState {
	[FacebookGUI instance].signInButton.title = @"Cancel";
	[[FacebookGUI instance].signInButton setEnabled:YES];
	[[FacebookGUI instance].fbProgress setHidden:NO];
	[[FacebookGUI instance].fbProgress startAnimation:self];
	[[FacebookGUI instance].fbStatusField setHidden:NO];
	[[FacebookGUI instance].fbStatus setTitle:@"Retrieving permissions..."];
	
	[[FacebookGUI instance].permissionButton setEnabled:NO];
	
	return self;
}

@end
