//
//  FacebookContext.h
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FBLoggedIn.h"
#import "FBLoggedOut.h"
#import "FBAuthenticating.h"
#import "FBRetrievingAlbums.h"
#import "FBRetrievingFriends.h"
#import "FBRetrievingUserInfo.h"
#import "FBRetrievingPermission.h"
#import "FBRetrievingPids.h"

@class FacebookState;

@interface FacebookContext : NSObject {
	FacebookState *state;
}

@property (readwrite, assign) FacebookState *state;

@end
