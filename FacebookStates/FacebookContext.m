//
//  FacebookContext.m
//  FacebookExporter
//
//  Created by Sean Farley on 3/23/09.
//  Copyright 2009 seanfarley.org. All rights reserved.
//

#import "FacebookContext.h"
#import "FacebookState.h"

@implementation FacebookContext

@synthesize state;

- (void)setState:(FacebookState *)newState {
	state = newState;
	[state changeState];
}

- init {
	if((self = [super init])) {
		self.state = [FacebookState instance];
	}
	
	return self;
}

@end
