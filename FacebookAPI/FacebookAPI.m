//
//  FacebookAPI.m
//  FacebookExporter
//
//  Created by Sean Farley on 2/13/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FacebookAPI.h"


@implementation FacebookAPI

+ facebookWithAPI: (NSString *)api delegate:(id)del selector:(SEL)sel {
	return [[[self alloc] initWithAPIKey:api delegate:del selector:sel] autorelease];
}

- initWithAPIKey: (NSString *)secret delegate:(id)del selector:(SEL)sel {
	return self;
}

- (NSWindow*)grantExtendedPermission:(NSString *)aString forSheet:(BOOL)forSheet delegate:(id)del selector:(SEL)sel {
	return nil;
}

#pragma mark -
#pragma mark Properties

- (NSString *)uid {
	return nil;
}

- (id)connection {
	return nil;
}

- (NSWindow*)login {
	return nil;
}

- (void)logout {
}

- (BOOL)userLoggedIn {
	return NO;
}

@end
