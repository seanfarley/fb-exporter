//
//  FacebookRequestAPI.m
//  FacebookExporter
//
//  Created by Sean Farley on 2/14/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FacebookRequestAPI.h"
#import "FacebookAPI.h"


@implementation FacebookRequestAPI

- init {
	self = [super init];
	if(self) {
		_delegate = nil;
		_selector = nil;
		_scheduleLoopIsModal = YES;
		_api = nil;
	}
	return self;
}

#pragma mark -
#pragma mark Properties

@synthesize delegate=_delegate, selector=_selector, scheduleLoopIsModal=_scheduleLoopIsModal, api=_api;

- (NSDictionary*)parameters {return nil;}
- (void)setParameters:(NSDictionary *)parameters {}
- (void)sendRequest {}
- (void)cancelRequest {}



@end
