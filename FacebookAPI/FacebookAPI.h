//
//  FacebookAPI.h
//  FacebookExporter
//
//  Created by Sean Farley on 2/13/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>


@interface FacebookAPI : NSObject {
	id _delegate;
	SEL _selector;
	id _perm_delegate;
	SEL _perm_selector;
}

@property (readonly) NSString *uid;
@property (readonly) id connection;
@property (readonly) NSWindow *login;
@property (readonly) BOOL userLoggedIn;

+ facebookWithAPI: (NSString *)api delegate:(id)del selector:(SEL)sel; 
- initWithAPIKey: (NSString *)api delegate:(id)del selector:(SEL)sel;
- (NSWindow*)grantExtendedPermission:(NSString *)aString forSheet:(BOOL)forSheet delegate:(id)del selector:(SEL)sel;
- (void)logout;

@end
