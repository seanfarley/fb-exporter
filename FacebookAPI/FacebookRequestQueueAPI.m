//
//  FacebookRequestQueueAPI.m
//  FacebookExporter
//
//  Created by Sean Farley on 2/14/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import "FacebookRequestQueueAPI.h"


@implementation FacebookRequestQueueAPI

- (void)startRequestQueue {}
- (void)cancelRequestQueue {}
- (void)addRequest:(FacebookRequestAPI *)request {}

#pragma mark -
#pragma mark Properties

@synthesize debugName=_debugName, api=_api, delegate=_delegate;
@synthesize currentlySendingSelector=_currentlySendingSelector;
@synthesize lastRequestResponseSelector=_lastRequestResponseSelector;
@synthesize allRequestsFinishedSelector=_allRequestsFinishedSelector;

- (NSUInteger)count {return 0;}

@end
