//
//  FacebookRequestAPI.h
//  FacebookExporter
//
//  Created by Sean Farley on 2/14/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class FacebookAPI;

@interface FacebookRequestAPI : NSObject {
	id _delegate;
	SEL _selector;
	BOOL _scheduleLoopIsModal;
	FacebookAPI *_api;
}

@property (readwrite, assign)	FacebookAPI *api;
@property (readwrite, assign)	id delegate;
@property (readwrite)			SEL selector;
@property (readwrite, copy)		NSDictionary *parameters;
@property (readwrite, assign)	BOOL scheduleLoopIsModal;

- (void)sendRequest;
- (void)cancelRequest;

@end
