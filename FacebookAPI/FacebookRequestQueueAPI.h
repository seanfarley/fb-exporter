//
//  FacebookRequestQueueAPI.h
//  FacebookExporter
//
//  Created by Sean Farley on 2/14/10.
//  Copyright 2010 LSU. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "FacebookRequestAPI.h"


@interface FacebookRequestQueueAPI : NSObject {
	id _delegate;
	SEL _currentlySendingSelector;
	SEL _lastRequestResponseSelector;
	SEL _allRequestsFinishedSelector;
	FacebookAPI *_api;
	
	NSString *_debugName;
}

@property (readwrite, assign)	FacebookAPI *api;
@property (readwrite, assign)	id delegate;
@property (readwrite, assign)	SEL currentlySendingSelector;
@property (readwrite, assign)	SEL lastRequestResponseSelector;
@property (readwrite, assign)	SEL allRequestsFinishedSelector;

@property (readonly)			NSUInteger count;
@property (copy)				NSString *debugName;

- (void)startRequestQueue;
- (void)cancelRequestQueue;
- (void)addRequest:(FacebookRequestAPI *)request;

@end
